# Datenschutzerklärung

Diese Datenschutzerklärung gillt für die Nutzung unseres Online-Angebots "TestApp" unter https://testapp.ga/ und die dazugehörigen Mobil-Apps (nachfolgend "Anwendung" oder "Website").

Jegliche Verarbeitung Ihrer personenbezogenen Daten (nachfolgend auch "Daten") geschieht unter Beachtung der geltenden datenschutzrechtlichen Vorschriften, insbesondere der Datenschutzgrundverordnung (DSGVO).

## 1) Verantwortlicher

Verantwortlicher für die Erhebung, Verarbeitung und Nutzung Ihrer personenbezogenen Daten im Sinne von Art. 4 Nr. 7 DSGVO ist

    Jasper Michalke
    August-Kirch-Straße 15j
    22525 Hamburg

    Email: info@testapp.ga

Wenn Sie der Erhebung, Verarbeitung oder Nutzung Ihrer Daten durch insgesamt oder für einzelne Maßnahmen widersprechen wollen, können Sie Ihren Widerspruch an den Verantwortlichen richten.

Sie können diese Datenschutzerklärung jederzeit speichern und ausdrucken.

## 2) Allgemeine Zwecke der Verarbeitung

Wir verarbeiten ihre Daten, damit Sie unsere Anwendung verwenden können sowie zur Abwehr unerwünschter Anfragen an unsere Server.

## 3) Welche Daten wir verwenden und warum

### 3.1) Protokolldaten

Durch die Verwendung unserer Anwendung werden folgende Daten erhoben und gespeichert:

 * Browsertyp und Browserversion
 * Betriebssystem
 * IP-Adresse

Ihre IP-Adresse wird annonymisiert. Das bedeutet, dass ein Teil von dieser abgeschnitten wird, damit diese nicht eindeutig zuordbar ist.

Wir nutzen diese Protokolldaten ohne Zuordnung zu Ihrer Person oder sonstiger Profilerstellung sondern nur Zwecks der Abwehr ungewollter Anfragen an unsere Server.

Hierin liegt auch unser berechtigtes Interesse gemäß Art 6 Abs. 1 S. 1 f) DSGVO.

### 3.2) Cookies

Wenn Sie unsere Webseite unter https://testapp.ga/ , https://dev.testapp.ga oder die Mobile-App für Windows 10 Betriebssysteme verwenden, verwenden wir sogenannte Session-Cookies, damit unsere Anwendung funktioniert.

Ein Session-Cookie ist eine kleine Textdatei, die von den jeweiligen Servern beim Besuch einer Internetseite verschickt und auf Ihrer Festplatte zwischengespeichert wird. Diese Datei als solche enthält eine sogenannte Session-ID, mit welcher sich verschiedene Anfragen Ihres Browsers der gemeinsamen Sitzung zuordnen lassen. Dadurch kann Ihr Rechner wiedererkannt werden, wenn Sie auf unsere Website zurückkehren. Diese Cookies werden gelöscht, nachdem Sie Ihren Browser schließen oder sich manuell von Ihrer Sitzung abmelden.

Außer Ihrer Session-Cookies werden keine personenbezogenen Cookies gespeichert.

Je nach Ihrer Einstellung werden alle Cookies nach 1:30 Stunden oder 12 Jahren gelöscht.

Auf die Nutzung von Cookies greifen wir nur bei angemeldeten Benutzern zurück.

Sie können Ihren Browser so einstellen, dass Sie über das Setzen von Cookies vorab informiert werden und im Einzelfall entscheiden können, ob Sie die Annahme von Cookies für bestimmte Fälle oder generell ausschließen, oder dass Cookies komplett verhindert werden. Dadurch wird jedoch das Anmelden an unserer Anwendung ausgeschlossen.

### 3.3) Nutzerkonto

Sie können in unsere Anwendung ein Nutzerkonto anlegen. Wünschen Sie dies, so verarbeiten wir die beim Login abgefragten personenbezogenen Daten. Beim späteren Einloggen werden nur Ihre Email-Adresse und das von Ihnen gewählte Passwort benötigt.

Für die Neuregistrierung erheben wir Name, Email-Adresse sowie ein von Ihnen gewähltes Passwort.

Erst nach erfolgter Registrierung speichern wir die von Ihnen übermittelten Daten dauerhaft in unserem System.

Wir möchten darauf hinweisen, dass Leitern von Kursen innerhalb unserer Anwendung möglich ist, die Zugangsdaten der Kursteilnehmer für kurze Zeit (maximal 30 Minuten) Mittels eines sog. OTP ("One time password", Einmal-Passwort) ausser Kraft zu setzen. Dies geschieht, um Zugang zu gewähren, wenn der Kursteilnehmer seine persönlichen Zugangdaten nicht vorliegen hat.

Sie können ein einmal angelegtes Nutzerkonto jederzeit von uns löschen lassen. Eine Mitteilung in Textform an die unter Ziffer 1 genannten Kontaktdaten (z.B. E-Mail, Fax, Brief) reicht hierfür aus. Wir werden dann Ihre gespeicherten personenbezogenen Daten löschen, soweit wir diese nicht noch aufgrund gesetzlicher Aufbewahrungspflichten speichern müssen.

Rechtgrundlage für die Verarbeitung dieser Daten ist Ihre Einwilligung gemäß Art. 6 Abs. 1 S. 1 a) DSGVO.

### 3.4) Email-Kontakt

Wenn Sie mit uns in Kontakt treten (z. B. per Kontaktformular oder E-Mail), verarbeiten wir Ihre Angaben zur Bearbeitung der Anfrage sowie für den Fall, dass Anschlussfragen entstehen.

Weitere personenbezogene Daten verarbeiten wir nur, wenn Sie dazu einwilligen (Art. 6 Abs. 1 S. 1 a) DSGVO) oder wir ein berechtigtes Interesse an der Verarbeitung Ihrer Daten haben (Art. 6 Abs. 1 S. 1 f) DSGVO). Ein berechtigtes Interesse liegt z. B. darin, auf Ihre E-Mail zu antworten.

### 4) Speicherdauer

Wir speichern Ihre Daten nur so lange, wie Sie es angeben oder wie es zur Erfüllung Ihrer Anfragen notwendig ist.

### 5) Ihre Rechte als von der Datenverarbeitung Betroffener

Nach den anwendbaren Gesetzen haben Sie verschiedene Rechte bezüglich Ihrer personenbezogenen Daten. Möchten Sie diese Rechte geltend machen, so richten Sie Ihre Anfrage bitte per E-Mail oder per Post unter eindeutiger Identifizierung Ihrer Person an die in Ziffer 1 genannte Adresse.

### 6) Datensicherheit

Wir sind um die Sicherheit Ihrer Daten maximal bemüht.

Ihre persönlichen Daten werden bei uns verschlüsselt übertragen. Dies gilt für Ihre Testergebnisse und -antworten und auch für den Login. Wir nutzen das Codierungssystem SSL (Secure Socket Layer), weisen jedoch darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.

Zur Sicherung Ihrer Daten unterhalten wir technische und organisatorische Sicherungsmaßnahmen entsprechend Art. 32 DSGVO, die wir immer wieder dem Stand der Technik anpassen.

Wir gewährleisten außerdem nicht, dass unser Angebot zu bestimmten Zeiten zur Verfügung steht; Störungen, Unterbrechungen oder Ausfälle können nicht ausgeschlossen werden. Die von uns verwendeten Server werden regelmäßig sorgfältig gesichert.

### 7) Weitergabe von Daten an Dritte

Grundsätzlich verwenden wir Ihre personenbezogenen Daten nur innerhalb unserer Anwendung.

Jedoch ist zu Beachten, dass die Leiter von Kursen innerhalb unserer Anwendung zugriff auf die Testergebnisse der einzelnen Kursteilnehmer, des gesamten Kurses sowie der gesamten Organisation (z. B. Schule oder Universität) haben.

Eine Datenübertragung an Stellen oder Personen außerhalb der EU findet nicht statt und ist nicht geplant.