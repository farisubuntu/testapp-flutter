# <img src="assets/bear-standard.png" height="56" width="56" /> TestApp Flutter

Learn better with TestApp!

![Current version](https://img.shields.io/badge/dynamic/yaml?label=Current%20version&query=version&url=https%3A%2F%2Fgitlab.com%2Ftestapp-system%2Ftestapp-flutter%2Fraw%2Fmobile%2Fpubspec.yaml%3Finline%3Dfalse&style=for-the-badge) 
![Gitlab pipeline status (mobile)](https://img.shields.io/gitlab/pipeline/testapp-system/testapp-flutter/mobile?style=for-the-badge)

[TestApp](https://testapp.ga/) is an educational open-source project for **writing class tests digitally** on smartphones or computers. We offer **easy-to-use class management**, **individual** class tests and random **practice** tests. Students can register **individually** or a **hole school/class** can use TestApp.

This project contains the Flutter client for TestApp. In oposite to the other platform-specific clients, this is the only one being continued. You can try it here: https://beta.testapp.ga/

Our source code is hosted on [GitLab](https://gitlab.com/testapp-system). Here, you can find our other clients too.

Proudly made with [![BrowserStack](assets/browserstack.svg)](https://www.browserstack.com/).

## General

### Goals

* **Statistics** for both students and teachers
* **Motivating** UI/UX
* Cross-platform
* **Mobile** apps, **Desktop** version and **Web** app
* **Secure** and **privacy** friendly
* *100% Open Source*
* Create scientific exercises using **LaTex** and **RegEx**
* Create **vocabulary** questions with multiple correct answers
* Support for **multiple choice** exercises
* **Random tests** and **class tests**

### Any suggestions?

No problem! Just open [a new issue](https://gitlab.com/groups/testapp-system/-/issues).

### Apps

* Windows 10: [Microsoft Store](https://www.microsoft.com/en-us/p/testapp-system/9p9jjfmff77d)
* Android:
    - [Play Store](https://play.google.com/store/apps/details?id=ga.testapp.testapp)
    - [F-Droid](https://f-droid.org/en/packages/ga.testapp.testapp/)
* iOS: [App Store](https://apps.apple.com/us/app/testapp-system/id1490425513)
* Desktop:
    - [Website](https://testapp.ga/static/downloads.html)
    - [GitLab](https://gitlab.com/testapp-system/testapp-electron/pipelines)
    - Linux: [Snap Store](https://snapcraft.io/testapp-desktop)
* Chrome OS: [Web Store](https://chrome.google.com/webstore/detail/testapp-system/hclopnbfffconajgdcmibjekjhmfegjf)

### Technical details

* Flutter [frontend](https://gitlab.com/testapp-system/testapp-flutter)
* PHP-written [back end](https://gitlab.com/testapp-system/testapp-backend) using MySQL/mariaDB

### Support development

You can support us by coding. Just ask to become member of [GitLab TestApp group](https://gitlab.com/testapp-system).

*For running our servers and deploying mobile apps we need $200. Help us!*

Feel free to donate: [Buy me a coffee](https://www.buymeacoffee.com/JasMich) or BTC: *3NUiJXDCkyRTb9Tg7n63yK6Y7CexADtSEh*

## Flutter client (this repository)

### Downloads and Versions

 * [![Gitlab pipeline status (mobile)](https://img.shields.io/gitlab/pipeline/testapp-system/testapp-flutter/mobile?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/testapp-system/testapp-flutter/-/jobs/artifacts/mobile/download?job=build%3Aapk)
 * [![F-Droid](https://img.shields.io/f-droid/v/ga.testapp.testapp?style=for-the-badge&logo=android&logoColor=white)](https://f-droid.org/en/packages/ga.testapp.testapp/)
 * [![Google Play](https://img.shields.io/endpoint?color=689f38&url=https%3A%2F%2Fplayshields.herokuapp.com%2Fplay%3Fi%3Dga.testapp.testapp%26l%3DGoogle-Play%26m%3D%24version&style=for-the-badge&logo=google-play&logoColor=white)](https://play.google.com/store/apps/details?id=ga.testapp.testapp)
 * [![Apple App Store](https://img.shields.io/itunes/v/1490425513?style=for-the-badge&logo=apple&logoColor=white)](https://apps.apple.com/us/app/testapp-system/id1490425513)

### Build

Install flutter first. See [flutter.dev](https://flutter.dev/docs/get-started/install) for more details.
```
# Run Flutter docter to check whether the installation was successfull
flutter doctor
```

Connect any Android or iOS device.

```
git clone https://gitlab.com/testapp-system/testapp-flutter.git
cd testapp-flutter
flutter run
```

### Screenshots

| ![](fastlane/metadata/android/en_US/images/phoneScreenshots/01.png) | ![](fastlane/metadata/android/en_US/images/phoneScreenshots/02.png) |
|---|---|
| ![](fastlane/metadata/android/en_US/images/phoneScreenshots/03.png) | ![](fastlane/metadata/android/en_US/images/phoneScreenshots/04.png) |
| ![](fastlane/metadata/android/en_US/images/phoneScreenshots/05.png) | ![](fastlane/metadata/android/en_US/images/phoneScreenshots/06.png) |
| ![](fastlane/metadata/android/en_US/images/phoneScreenshots/07.png) | ![](fastlane/metadata/android/en_US/images/phoneScreenshots/08.png) |

### Web support

Since version 3.1.0 we support Flutter for web. You can try the beta frontend at https://beta.testapp.ga/

### Desktop support

Desktops (specially macOS due to Flutter's development state) are supported by this project. Expect problems as LaTeX code cannot be rendered yet. If you want, you may have a look at our LaTeX compile at https://gitlab.com/testapp-system/katex_flutter/ .

## License

TestApp is licensed under the `EUPL-1.2`.
