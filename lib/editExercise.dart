import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/exercises.dart';
import 'package:test_app_flutter/style.dart';
import 'package:url_launcher/url_launcher.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class EditExercise extends StatefulWidget {
  final int id;

  EditExercise({this.id});

  @override
  _EditExerciseState createState() => _EditExerciseState();
}

class _EditExerciseState extends State<EditExercise> {
  bool _loading = true;
  Map exerciseData;

  bool _exerciseActivated = true;
  List _answers = [];
  String _awType = 'multipleChoise';
  Set _selectedTopics = Set();

  Widget _image = Container();

  TextEditingController _nameController = TextEditingController(),
      _contentController = TextEditingController();

  List<TextEditingController> _answerController =
      List.generate(5, (i) => TextEditingController());
  List<TextEditingController> _answerRegExHumanReadableController =
      List.generate(5, (i) => TextEditingController());
  bool _answerOrderMatters = false;
  List<int> _selectedCorrectAnswers = [];

  bool _activationLoading = false;
  DateTime _exerciseDisactivationDate = DateTime.now();

  @override
  void initState() {
    // if not a new exercise
    if (widget.id != null) {
      globals.api
          .call('fetchExercises', {'exercise': widget.id, 'images': true}).then(
              (data) {
        setState(() {
          exerciseData = data['response'][0];
          _exerciseActivated =
              (int.parse(exerciseData['deactivate'].toString()) <=
                  DateTime.now().millisecondsSinceEpoch / 1000);
          if (!_exerciseActivated)
            _exerciseDisactivationDate = DateTime.fromMillisecondsSinceEpoch(
                int.parse(exerciseData['deactivate'] + '000'));

          for (int i = 1; i < 6; i++) {
            if (exerciseData["aw$i"] != "") _answers.add(exerciseData["aw$i"]);
          }

          //Exercise type
          switch (exerciseData['awtype']) {
            case 'number':
            case 'text':
            case 'inputMultiple':
            case 'fillInText':
              _awType = 'fillInText';
              break;
            case 'radio':
            case 'checkbox':
              _awType = 'multipleChoise';
              List correctAnswers = jsonDecode(exerciseData['correct']);
              for (int i = 0; i < _answers.length; i++) {
                if (correctAnswers.contains(_answers[i]))
                  _selectedCorrectAnswers.add(i);
              }
              break;
            case 'regex':
            case 'multipleRegEx':
            case 'fillRegEx':
              _awType = 'fillInRegEx';
              // Adding regex' to their controllers
              List regex = jsonDecode(exerciseData['correct']);
              for (int i = 0; i < regex.length; i++) {
                _answerRegExHumanReadableController[i].text = regex[i];
              }
              break;
          }

          // Adding answers to their controllers
          for (int i = 0; i < _answers.length; i++) {
            _answerController[i].text = _answers[i];
          }

          _nameController.text = exerciseData['name'];
          _contentController.text = exerciseData['content'];

          _selectedTopics = Set.from(jsonDecode(exerciseData['topic']));

          if (exerciseData['img'] != null && exerciseData['img'] != "") {
            final UriData data = Uri.parse(exerciseData['img']).data;
            _image = Container(
                child: Image.memory(
              data.contentAsBytes(),
              scale: 2,
            ));
          } else {
            _image = Container();
          }

          _loading = false;
        });
      });
    } else {
      _loading = false;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // Answer list
    Widget _answerList;

    switch (_awType) {
      case 'fillInText':
        _answerList = ListView.separated(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            primary: false,
            itemBuilder: (c, index) {
              if (index == 0)
                return (SwitchListTile(
                  value: _answerOrderMatters,
                  title: Text(S.of(context).answerOrderMatters),
                  onChanged: (selected) => setState(() {
                    _answerOrderMatters = selected;
                  }),
                ));
              return (ListTile(
                title: TextField(
                  controller: _answerController[index - 1],
                  decoration: InputDecoration(
                      labelText: S.of(context).answer + " $index"),
                ),
              ));
            },
            separatorBuilder: (c, i) => Divider(),
            itemCount: 5 + 1);
        break;
      case 'multipleChoise':
        _answerList = ListView.separated(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            primary: false,
            itemBuilder: (c, index) {
              return (CheckboxListTile(
                value: _selectedCorrectAnswers.contains(index),
                onChanged: (selected) {
                  setState(() {
                    (selected)
                        ? _selectedCorrectAnswers.add(index)
                        : _selectedCorrectAnswers.remove(index);
                  });
                },
                title: TextField(
                  controller: _answerController[index],
                  decoration: InputDecoration(
                      labelText:
                          S.of(context).multiplechoiseOption + " ${index + 1}"),
                ),
              ));
            },
            separatorBuilder: (c, i) => Divider(),
            itemCount: 5);
        break;
      case 'fillInRegEx':
        _answerList = ListView.separated(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            primary: false,
            itemBuilder: (c, index) {
              if (index == 0)
                return (SwitchListTile(
                  value: _answerOrderMatters,
                  title: Text(S.of(context).answerOrderMatters),
                  onChanged: (selected) => setState(() {
                    _answerOrderMatters = selected;
                  }),
                ));
              if (index == 1)
                return (ListTile(
                  title: RichText(
                      text: TextSpan(
                    style: TextStyle(color: Colors.black),
                    children: [
                      TextSpan(
                        text: S.of(context).important,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      TextSpan(
                          text: S
                              .of(context)
                              .testYourRegexBeforeSavingWeRecommendTheOpensourceTool),
                    ],
                  )),
                  trailing: OutlineButton(
                    onPressed: () => launch('https://regex101.com/'),
                    child: Text('regex101'),
                  ),
                ));
              return (ListTile(
                title: Wrap(spacing: 18, runSpacing: 16, children: [
                  Container(
                    constraints: BoxConstraints(maxWidth: 256),
                    child: TextField(
                      controller: _answerController[index - 2],
                      decoration: InputDecoration(
                          labelText: S.of(context).regex + " ${index - 1}",
                          helperText: S.of(context).noDelimiterRequired),
                    ),
                  ),
                  Container(
                    constraints: BoxConstraints(maxWidth: 256),
                    child: TextField(
                      controller:
                          _answerRegExHumanReadableController[index - 2],
                      decoration: InputDecoration(
                          labelText: S.of(context).answer + " ${index - 1}",
                          helperText: S.of(context).humanreadableVersion),
                    ),
                  ),
                ]),
              ));
            },
            separatorBuilder: (c, i) => Divider(),
            itemCount: 5 + 2);
        break;
    }
    return (ResponsiveDrawerScaffold(
      title: S.of(context).editExericse,
      appBarLeading: IconButton(
        icon: Icon(Icons.close),
        onPressed: () => Navigator.of(context).pop(),
        tooltip: S.of(context).close,
      ),
      appBarActions: (!_loading && widget.id != null)
          ? [
              IconButton(
                icon: Icon(Icons.delete_forever),
                onPressed: deleteExercise,
                tooltip: S.of(context).deleteThisExercise,
              ),
            ]
          : [],
      body: (!_loading)
          ? ListView(children: [
              TestAppCard(
                children: <Widget>[
                  Text(S.of(context).exercise,
                      style: Theme.of(context).textTheme.headline6),
                  TextField(
                    controller: _nameController,
                    decoration:
                        InputDecoration(labelText: S.of(context).exerciseName),
                  ),
                  TextField(
                    controller: _contentController,
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    decoration:
                        InputDecoration(labelText: S.of(context).exercise),
                  )
                ],
              ),
              TestAppCard(
                children: <Widget>[
                  Text(
                    S.of(context).answers,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  Center(
                    child: CupertinoSegmentedControl(
                      borderColor: Theme.of(context).accentColor,
                      selectedColor: Theme.of(context).accentColor,
                      children: {
                        'multipleChoise': Padding(
                          child: Text(S.of(context).multipleChoise),
                          padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                        ),
                        'fillInText': Padding(
                          child: Text(S.of(context).input),
                          padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                        ),
                        'fillInRegEx': Padding(
                          child: Text(S.of(context).inputUsingRegex),
                          padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                        ),
                      },
                      onValueChanged: (newVal) {
                        setState(() {
                          _awType = newVal;
                        });
                      },
                      groupValue: _awType,
                      padding: EdgeInsets.all(8),
                    ),
                  ),
                  Divider(),
                  Text(S.of(context).pleaseJustLeaveEmptyUnusedTextFields),
                  _answerList
                ],
              ),
              TestAppCard(children: <Widget>[
                Text(S.of(context).topics,
                    style: Theme.of(context).textTheme.headline6),
                TopicSelector(
                    onSelect: (newTopics) => {_selectedTopics = newTopics}),
              ]),
              (widget.id != null)
                  ? TestAppCard(
                      children: <Widget>[
                        Text(
                          S.of(context).exerciseActivation,
                          style: Theme.of(context).textTheme.headline6,
                        ),
                        Text(S.of(context).ifYouPlanToWriteAJoinTestYouCan),
                        (_exerciseActivated)
                            ? Container(
                                child: ListTile(
                                  leading: (_activationLoading)
                                      ? CircularProgressIndicator()
                                      : Icon(FontAwesomeIcons.lockOpen),
                                  onTap: _disableExercise,
                                  title: Text(S
                                      .of(context)
                                      .thisExerciseIsActivatedTapToDisable),
                                ),
                              )
                            : ListTile(
                                leading: (_activationLoading)
                                    ? CircularProgressIndicator()
                                    : Icon(FontAwesomeIcons.userLock),
                                onTap: _activateExercise,
                                title: Text(S
                                        .of(context)
                                        .thisExerciseIsDisabledTill +
                                    ' ${_exerciseDisactivationDate.day.toString() + '.' + _exerciseDisactivationDate.month.toString() + '.' + _exerciseDisactivationDate.year.toString()}. ' +
                                    S.of(context).tapToActivate),
                              )
                      ],
                    )
                  : Container(),
              TestAppCard(
                children: <Widget>[
                  Text(
                    S.of(context).image,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  Text(S
                      .of(context)
                      .editingImagesIsNotYetSupportedOnMobilePlatforms),
                  Container(
                    child: _image,
                    color: Colors.green,
                  )
                ],
              ),
              TestAppCard(
                children: <Widget>[
                  Text(S.of(context).iOwnTheCopyrightOfThisExercise),
                  Text(S.of(context).bySavingThisExerciseIAsignAllRightsOfThis),
                  Row(
                    children: <Widget>[
                      OutlineButton(
                        onPressed: () =>
                            launch(S.of(context).creativeCommonsUrl),
                        child: Row(
                          children: <Widget>[
                            Icon(FontAwesomeIcons.creativeCommons),
                            Icon(FontAwesomeIcons.creativeCommonsBy),
                            Icon(FontAwesomeIcons.creativeCommonsSa),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              )
            ])
          : CenterProgress(),
      floatingActionButton: (!_loading)
          ? FloatingActionButton.extended(
              onPressed: saveExercise,
              label: Text(S.of(context).saveExercise),
              icon: Icon(Icons.check),
            )
          : FloatingActionButton(
              onPressed: () {},
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.white),
              ),
            ),
    ));
  }

  void saveExercise() {
    Map<String, dynamic> options = Map();
    options['name'] = _nameController.text.trim();
    options['content'] = _contentController.text.trim();

    // Cleaning answers
    List answers = _answerController.map((controller) {
      return (controller.text.trim());
    }).toList();

    // Checking the correct answers
    switch (_awType) {
      case 'fillInRegEx':
        List regex = _answerRegExHumanReadableController.map((controller) {
          return (controller.text.trim());
        }).toList();
        options['answers'] = regex.where((el) => el != '').toList();

        if (_answerOrderMatters) {
          options['type'] = 'fillInRegEx';
        } else {
          options['type'] = 'multipleRegEx';
          answers.sort();
        }

        options['correct'] = answers.where((el) => el != '').toList();

        break;

      case 'fillInText':
        options['correct'] = answers.where((el) => el != '').toList();

        if (_answerOrderMatters) {
          options['type'] = 'fillInText';
        } else {
          options['type'] = 'inputMultiple';
          answers.sort();
        }

        options['answers'] = answers.where((el) => el != '').toList();

        break;

      case 'multipleChoise':
        List correct = [];
        _selectedCorrectAnswers.forEach((correctAnswerId) {
          // Using controller instead of existing answer List because there may be empty answers
          correct.add(_answerController[correctAnswerId].text.trim());
        });

        if (correct.length == 1) {
          options['type'] = 'radio';
        } else {
          options['type'] = 'checkbox';
        }

        options['answers'] = answers.where((el) => el != '').toList();

        break;
    }

    // Adding topics
    options['topics'] = _selectedTopics.toList();

    if (!(_selectedTopics.isNotEmpty &&
        options['name'].isNotEmpty &&
        options['content'].isNotEmpty &&
        options['answers'].isNotEmpty &&
        options['correct'].isNotEmpty &&
        options['topics'].isNotEmpty)) {
      /*Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Please fill in all required fields.'),
      ));*/
      _loading = false;
      return;
    }

    String job = 'addExercise';
    if (widget.id != null) {
      options['exercise'] = widget.id;
      job = 'updateExercise';
    }

    globals.api.call(job, options).then((data) {
      setState(() {
        _loading = false;
      });
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (b) => Exercises()));
    });
    setState(() {
      _loading = true;
    });
  }

  void deleteExercise() {
    showDialog(
        context: context,
        child: AlertDialog(
          content: Text(S.of(context).areYouSureToDeleteThisExercise),
          actions: <Widget>[
            MaterialButton(
              onPressed: Navigator.of(context).pop,
              child: Text(S.of(context).cancel),
            ),
            MaterialButton(
                onPressed: () {
                  setState(() {
                    _loading = true;
                  });
                  globals.api.call(
                      'deleteExercise', {'exercise': widget.id}).then((data) {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (b) => Exercises()));
                  });
                },
                child: Text(S.of(context).yes)),
          ],
        ));
  }

  void _disableExercise() {
    showDatePicker(
            context: context,
            initialDate: DateTime.now().add(Duration(days: 3)),
            firstDate: DateTime.now(),
            lastDate: DateTime.now().add(Duration(days: 14)))
        .then((selectedDate) {
      if (selectedDate == null) return;
      _exerciseDisactivationDate = selectedDate;
      var timestamp = selectedDate.millisecondsSinceEpoch.toString().substring(
          0, selectedDate.millisecondsSinceEpoch.toString().length - 3);

      setState(() {
        _activationLoading = true;
      });
      globals.api.call('exerciseActivation', {
        "exercise": exerciseData['id'],
        'activation': timestamp
      }).then((data) {
        if (data['response']) {
          _exerciseActivated = !_exerciseActivated;
        }
        setState(() {
          _activationLoading = false;
        });
      });
    });
  }

  void _activateExercise() {
    setState(() {
      _activationLoading = true;
    });
    globals.api.call('exerciseActivation',
        {"exercise": exerciseData['id'], 'activation': 0}).then((data) {
      if (data['response']) {
        _exerciseActivated = !_exerciseActivated;
      }
      setState(() {
        _activationLoading = false;
      });
    });
  }
}

class TopicSelector extends StatefulWidget {
  final Function onSelect;

  TopicSelector({Key key, this.onSelect}) : super(key: key);

  @override
  _TopicSelectorState createState() => _TopicSelectorState();
}

class _TopicSelectorState extends State<TopicSelector>
    with AutomaticKeepAliveClientMixin<TopicSelector> {
  bool _topicsLoaded = false;
  Set _selectedTopics = Set();
  Map topicNames = {};
  Map topics;

  TextEditingController _addTopicName = TextEditingController();
  TextEditingController _addTopicSubject = TextEditingController();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    _refreshTopicList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    Widget dropDownButton = DropdownButton<int>(
      //value: null,
      //icon: Icon(FontAwesomeIcons.school),
      iconSize: 24,
      elevation: 16,
      //itemHeight: 48,
      hint: Text(S.of(context).addANewTopic),
      style: TextStyle(color: Colors.lightBlue),
      underline: Container(
        height: 2,
        color: Colors.green,
      ),
      onChanged: (int newValue) {
        setState(() {
          _selectedTopics.add(newValue);
        });
        widget.onSelect(_selectedTopics);
      },
      items: topicNames.keys.map((key) {
        return DropdownMenuItem<int>(
          value: key,
          child: Text(topicNames[key]),
        );
      }).toList(),
    );
    return (_topicsLoaded)
        ? Column(
            children: <Widget>[
              Wrap(spacing: 8.0, runSpacing: 4.0, children: [
                dropDownButton,
                FloatingActionButton(
                  heroTag: 'addTopicFAB',
                  // Preventing crashes for animations when navigating
                  onPressed: _createNewTopic,
                  tooltip: S.of(context).createNewTopic,
                  child: Icon(Icons.add),
                  elevation: 2,
                  mini: true,
                )
              ]),
              Wrap(
                spacing: 8.0, // gap between adjacent chips
                runSpacing: 4.0, // gap between lines
                children: _selectedTopics.map((topicId) {
                  return (Chip(
                      onDeleted: () {
                        setState(() {
                          _selectedTopics.remove(int.parse(topicId.toString()));
                        });
                        widget.onSelect(_selectedTopics);
                      },
                      label: Text(topicNames[int.parse(topicId.toString())])));
                }).toList(),
              ),
            ],
          )
        : CenterProgress();
  }

  void _createNewTopic() {
    showDialog(
        context: context,
        child: AlertDialog(
          title: Text(
            S.of(context).createNewTopic,
          ),
          content: Column(
            children: <Widget>[
              TextField(
                controller: _addTopicName,
                decoration: InputDecoration(labelText: S.of(context).topicName),
              ),
              TextField(
                controller: _addTopicSubject,
                decoration: InputDecoration(labelText: S.of(context).subject),
              ),
            ],
          ),
          actions: <Widget>[
            MaterialButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).cancel),
            ),
            MaterialButton(
              onPressed: () {
                setState(() {
                  _topicsLoaded = false;
                });
                globals.api.call('addTopic', {
                  'name': _addTopicName.text,
                  'subject': _addTopicSubject.text
                }).then((value) {
                  _refreshTopicList();
                });
                Navigator.of(context).pop();
              },
              child: Text(S.of(context).create),
            )
          ],
        ));
  }

  void _refreshTopicList() {
    globals.api.call('listTopics').then((data) {
      List rawTopics = data['response'];
      Map subjects = Map();
      rawTopics.forEach((currentTopic) {
        if (!subjects.keys.contains(currentTopic['subject']))
          subjects[currentTopic['subject']] = List();
        subjects[currentTopic['subject']].add(currentTopic);
        // For name and id
        topicNames[int.parse(currentTopic['id'].toString())] =
            currentTopic['name'];
      });
      setState(() {
        _topicsLoaded = true;
        topics = subjects;
      });
    });
  }
}
