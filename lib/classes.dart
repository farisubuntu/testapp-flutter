import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/classScore.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/editClass.dart';
import 'package:test_app_flutter/style.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class Classes extends StatefulWidget {
  @override
  _ClassesState createState() => _ClassesState();
}

class _ClassesState extends State<Classes> {
  List _classes = [];
  bool _classesLoaded = false;
  TextEditingController _newClassInput = TextEditingController();

  @override
  void initState() {
    globals.api.call('listClasses').then((data) {
      if (data['response'] is List) _classes = data['response'];
      setState(() {
        _classesLoaded = true;
      });
    });
    super.initState();
  }

  void createClass() async {
    showDialog(
        context: context,
        child: Dialog(
          child: Container(
            constraints:
                BoxConstraints(minWidth: 280, maxWidth: 560, minHeight: 182),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    S.of(context).createClass,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  RichText(
                      text: TextSpan(
                    style: TextStyle(color: Colors.black),
                    children: [
                      TextSpan(
                        text: S.of(context).hint,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      TextSpan(
                          text: S
                              .of(context)
                              .thinkAboutTheNameSometimesItsMoreSensfulToGive),
                      TextSpan(
                          text: '10b',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(
                          text:
                              S.of(context).butAnywaySometimesYouNeedNamesLike),
                      TextSpan(
                          text: S.of(context).exampleClassName,
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(
                          text: S
                              .of(context)
                              .youShouldTalkAboutThisWithYourColleagues)
                    ],
                  )),
                  TextField(
                    controller: _newClassInput,
                    decoration:
                        InputDecoration(labelText: S.of(context).className),
                    autofocus: true,
                  ),
                  ButtonBar(
                    children: <Widget>[
                      MaterialButton(
                        onPressed: () => Navigator.pop(context),
                        child: Text(S.of(context).cancel),
                      ),
                      RaisedButton(
                        onPressed: () {
                          globals.api.call('addClass',
                              {'name': _newClassInput.text}).then((data) {
                            initState();
                          });
                          Navigator.of(context).pop();
                          setState(() {
                            _classesLoaded = false;
                          });
                        },
                        child: Text(S.of(context).createClass),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveDrawerScaffold(
      body: Column(
        children: <Widget>[
          TestAppCard(
            children: <Widget>[
              Text(
                S.of(context).myClasses,
                style: Theme.of(context).textTheme.headline6,
              ),
              (_classesLoaded)
                  ? (_classes.length > 0)
                      ? ListView.separated(
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            return ListTile(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ClassScore(
                                          classId: int.parse(
                                              _classes[index]['id']))),
                                );
                              },
                              leading: Icon(
                                FontAwesomeIcons.chartPie,
                              ),
                              title: Text(_classes[index]['name']),
                              trailing: IconButton(
                                  tooltip: 'Edit class \"' +
                                      _classes[index]['name'] +
                                      '\"',
                                  icon: Icon(Icons.edit),
                                  onPressed: () {
                                    Navigator.of(context)
                                        .push(MaterialPageRoute(
                                            builder: (b) => EditClass(
                                                  id: int.parse(
                                                      _classes[index]['id']),
                                                  classData: _classes[index],
                                                )));
                                  }),
                            );
                          },
                          separatorBuilder: (context, index) => Divider(),
                          itemCount: _classes.length)
                      : ListTile(
                          title: Text(S.of(context).noClassesCreatedYet),
                          trailing: RaisedButton(
                            child: Text(
                              S.of(context).createClass,
                            ),
                            onPressed: createClass,
                          ),
                        )
                  : CenterProgress()
            ],
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: createClass,
        tooltip: S.of(context).createNewClass,
      ),
    );
  }
}
