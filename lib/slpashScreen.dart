import 'package:flutter/material.dart';
import 'package:test_app_flutter/api.dart';
import 'package:test_app_flutter/dashboard.dart';
import 'package:test_app_flutter/login.dart';
import 'package:test_app_flutter/style.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  var api = globals.api;
  bool isAdmin = false;

  Future adminCheck() {
    return api.call('userInfo').then((data) {
      Preferences().save("Admin",
          (int.parse(data['response']['accesslevel']) >= 1).toString());
    });
  }

  @override
  void initState() {
    super.initState();
    api.initializationDone.then((newVal) {
      api.call('loginState').then((data) async {
        if (data["response"] == true) {
          try {
            // If information stored, only verifying again
            await Preferences().fetch("sid");
            adminCheck();
          } catch (e) {
            // If no admin info saved, **awaiting** fetching them
            await adminCheck();
          }
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => Dashboard(
                      title: "TestApp",
                    )),
          );
        } else {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => LoginPage()),
          );
        }
      }).catchError((error) {
        print('API Error: ' + error.toString());
      });
    });
    Preferences().fetch('language').then((languageCode) {
      switch (languageCode) {
        case 'en':
          S.load(Locale('en', 'EU'));
          break;

        case 'de':
          S.load(Locale('de', 'DE'));
          break;

        case 'fr':
          S.load(Locale('fr', 'FR'));
          break;

        case 'en':
          S.load(Locale('tlh', 'QS'));
          break;
        default:
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: SingleChildScrollView(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Image.asset('assets/bears/bear-standard.png',
                scale: (MediaQuery.of(context).size.width < 768) ? 1 : 1.5),
            CenterProgress(),
          ],
        ),
      )),
    ));
  }
}
