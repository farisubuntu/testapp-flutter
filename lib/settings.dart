import 'package:flutter/material.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/login.dart';
import 'package:test_app_flutter/style.dart';

import 'api.dart';
import 'generated/l10n.dart';
import 'globals.dart' as globals;

class SettingsPage extends StatefulWidget {
  SettingsPage({Key key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  var api = globals.api;
  TextEditingController _passwordResetController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ResponsiveDrawerScaffold(
        body: ListView(children: [
      TestAppCard(children: [
        Text(
          S.of(context).account,
          style: Theme.of(context).textTheme.headline6,
        ),
        ListTile(
          title: Text(S.of(context).logOut),
          onTap: _logOut,
        ),
        ListTile(
          title: Text(
            S.of(context).updatePassword + '...',
          ),
          onTap: _updatePassword,
        ),
      ]),
      TestAppCard(children: [
        Text(
          'App',
          style: Theme.of(context).textTheme.headline6,
        ),
        ListTile(
          leading: Icon(Icons.language),
          title: Text('Language'),
          onTap: () {
            showDialog(
                context: context,
                child: SimpleDialog(
                  title: Text('Language'),
                  children: <Widget>[
                    SimpleDialogOption(
                      onPressed: () {
                        setState(() {
                          S.load(Locale('en', 'EU'));
                          Preferences().save('language', 'en');
                        });
                        Navigator.of(context).pop();
                      },
                      child: ListTile(
                        title: Text('English'),
                        leading: Image.asset('assets/languages/europe.png'),
                      ),
                    ),
                    SimpleDialogOption(
                      onPressed: () {
                        setState(() {
                          S.load(Locale('de', 'DE'));
                          Preferences().save('language', 'de');
                        });
                        Navigator.of(context).pop();
                      },
                      child: ListTile(
                        title: Text('Deutsch'),
                        leading: Image.asset('assets/languages/germany.png'),
                      ),
                    ),
                    SimpleDialogOption(
                      onPressed: () {
                        setState(() {
                          S.load(Locale('efrn', 'FR'));
                          Preferences().save('language', 'fr');
                        });
                        Navigator.of(context).pop();
                      },
                      child: ListTile(
                        title: Text('Français'),
                        leading: Image.asset('assets/languages/france.png'),
                      ),
                    ),
                    SimpleDialogOption(
                      onPressed: () {
                        setState(() {
                          S.load(Locale('tlh', 'QS'));
                          Preferences().save('language', 'tlh');
                        });
                        Navigator.of(context).pop();
                      },
                      child: ListTile(
                        title: Text('tlhIngan Hol'),
                        leading: FloatingActionButton(
                          heroTag: 'tlh',
                          backgroundColor: Colors.red,
                          elevation: 0,
                          onPressed: () {},
                          child: Image.asset('assets/languages/klingon.png'),
                        ),
                      ),
                    ),
                  ],
                ));
          },
        )
      ])
    ]));
  }

  void _logOut() {
    api.call('logout').then((data) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => LoginPage()),
          (Route<dynamic> route) => false);
    });
  }

  void _updatePassword() {
    showDialog(
        context: context,
        child: AlertDialog(
          title: Text(
            S.of(context).updatePassword,
            style: Theme.of(context).textTheme.headline6,
          ),
          content: Column(
            children: <Widget>[
              Text(S
                  .of(context)
                  .pleaseProvideANewPasswordWeRecommendChoosingAPassword),
              TextField(
                controller: _passwordResetController,
                decoration: InputDecoration(
                  labelText: S.of(context).newPassword,
                ),
                obscureText: true,
                autofocus: true,
              ),
            ],
          ),
          actions: <Widget>[
            MaterialButton(
              child: Text(S.of(context).cancel),
              onPressed: () => Navigator.pop(context),
            ),
            MaterialButton(
              child: Text(S.of(context).updatePassword),
              onPressed: () {
                api.call('updateUser',
                    {'pass': _passwordResetController.text}).then((data) {
                  Navigator.pop(context);
                });
              },
            )
          ],
        ));
  }
}
