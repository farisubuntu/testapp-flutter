import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/classScore.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/joinTests.dart';
import 'package:test_app_flutter/style.dart';
import 'package:test_app_flutter/writeTest.dart';

import 'exercises.dart';
import 'generated/l10n.dart';
import 'globals.dart' as globals;

class EditJoinTest extends StatefulWidget {
  final int id;
  final Map testData;

  EditJoinTest({this.id, this.testData});

  @override
  _EditJoinTestState createState() => _EditJoinTestState();
}

class _EditJoinTestState extends State<EditJoinTest> {
  bool _dataAvailable = false;
  bool _newTest = false;
  TextEditingController _testNameController = TextEditingController();
  ExercisesSelection _selectedExercises = ExercisesSelection();
  bool _savingTest = false;
  bool _loadWriteTest = false;
  Map _testData;

  @override
  void initState() {
    if (widget.testData == null && widget.id != null) {
      globals.api.call('listJoin', {'showAll': true}).then((data) {
        if (data['response'] is List) {
          List _joinTests = data['response'];
          _testData = _joinTests.firstWhere((elem) => (elem.id == widget.id),
              orElse: () {
            Navigator.of(context).pop();
          });
          setState(() {
            init();
          });
        }
      });
    } else {
      _testData = widget.testData;
      init();
    }
    super.initState();
  }

  void init() {
    if (widget.id != null) {
      _selectedExercises.addAll(jsonDecode(_testData['exercises']));
      _testNameController.text = _testData['name'];
    } else
      _newTest = true;
    setState(() {
      _dataAvailable = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return (ResponsiveDrawerScaffold(
      appBarLeading: IconButton(
          icon: Icon(Icons.close), onPressed: Navigator.of(context).pop),
      appBarActions: (!_newTest)
          ? <Widget>[
              IconButton(
                icon: Icon(
                  FontAwesomeIcons.clone,
                  size: 18.0,
                ),
                onPressed: () {
                  _newTest = true;
                },
                tooltip: S.of(context).forkThisTest,
              ),
              IconButton(
                icon: Icon(Icons.delete_forever),
                onPressed: deleteTest,
                tooltip: S.of(context).deleteThisJoinTest,
              )
            ]
          : null,
      title: S.of(context).editJoinTest,
      body: (_dataAvailable)
          ? ListView(
              children: <Widget>[
                TestAppCard(
                  children: <Widget>[
                    Text(
                      S.of(context).joinTestName,
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    TextField(
                      controller: _testNameController,
                      decoration: InputDecoration(
                          labelText: S.of(context).joinTestName),
                    )
                  ],
                ),
                TestAppCard(
                  children: <Widget>[
                    Text(
                      S.of(context).exercises,
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    if (_dataAvailable)
                      Text("${_selectedExercises.toSet().length.toString()} " +
                          S.of(context).exercisesSelected),
                    (_dataAvailable)
                        ? Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: OutlineButton.icon(
                                icon: Icon(FontAwesomeIcons.graduationCap),
                                onPressed: selectExercises,
                                label: Text(S.of(context).pickExercises)),
                          )
                        : CenterProgress()
                  ],
                ),
                if (widget.id != null)
                  TestAppCard(
                    children: <Widget>[
                      Text(
                        S.of(context).writeTest,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      (_dataAvailable)
                          ? Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: OutlineButton.icon(
                                  icon: (!_loadWriteTest)
                                      ? Icon(FontAwesomeIcons.penNib)
                                      : Container(
                                          height: 18,
                                          width: 18,
                                          child: CircularProgressIndicator(
                                            valueColor: AlwaysStoppedAnimation(
                                                Colors.green),
                                          ),
                                        ),
                                  onPressed: writeJoinTest,
                                  label: Text(S.of(context).writeThisTest)),
                            )
                          : CenterProgress()
                    ],
                  ),
              ],
            )
          : CenterProgress(),
      floatingActionButton: (!_savingTest)
          ? FloatingActionButton.extended(
              onPressed: saveTest,
              label: Text(S.of(context).saveJoinTest),
              icon: Icon(Icons.check),
            )
          : FloatingActionButton(
              onPressed: () {},
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.white),
              ),
            ),
    ));
  }

  void selectExercises() async {
    ExercisesSelection exercises = await Navigator.of(context)
        .push(new MaterialPageRoute<ExercisesSelection>(
            builder: (BuildContext context) {
              return Exercises(
                  selectExercises: true, selectedExercises: _selectedExercises);
            },
            fullscreenDialog: true));
    if (exercises != null) setState(() => _selectedExercises = exercises);
  }

  void saveTest() {
    if (_testNameController.text.trim() != '' &&
        _selectedExercises.toSet().length != 0) {
      setState(() {
        _savingTest = true;
      });
      Map<String, dynamic> options = {
        'exercises': _selectedExercises.toSet().toList(),
        'name': _testNameController.text.trim()
      };
      if (!_newTest) options['id'] = widget.id;
      String command = (!_newTest) ? 'updateJoin' : 'addJoin';
      globals.api.call(command, options).then((data) {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (c) => JoinTests()));
      });
    } else {
      Scaffold.of(context).showSnackBar(
          SnackBar(content: Text(S.of(context).pleaseFillInAllFields)));
    }
  }

  void deleteTest() {
    showDialog(
        context: context,
        child: AlertDialog(
          content: Text(S.of(context).areYouSureToDeleteThisJoinTest),
          actions: [
            MaterialButton(
              onPressed: Navigator.of(context).pop,
              child: Text('Cancel'),
            ),
            MaterialButton(
                onPressed: () {
                  setState(() {
                    _savingTest = true;
                  });
                  globals.api
                      .call('deleteJoin', {'id': widget.id}).then((data) {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (b) => JoinTests()));
                  });
                },
                child: Text(S.of(context).yes)),
          ],
        ));
  }

  void writeJoinTest() async {
    setState(() {
      _loadWriteTest = true;
    });
    TextEditingController _testTimeController =
        TextEditingController(text: '10');
    TextEditingController _testExpiryController =
        TextEditingController(text: '30');

    Map response = await globals.api.call('listClasses', {'showAll': false});
    List classes = response['response'];

    int _selectedClass;

    Widget classesDropdown = Row(children: [
      Text(S.of(context).wordClass + ': '),
      DropdownButton<int>(
        value: _selectedClass,
        icon: Icon(FontAwesomeIcons.users),
        iconSize: 24,
        elevation: 16,
        //itemHeight: 48,
        style: TextStyle(color: Colors.lightBlue),
        underline: Container(
          height: 2,
          color: Colors.green,
        ),
        onChanged: (int newValue) {
          setState(() {
            _selectedClass = newValue;
          });
        },
        items: classes.map((currentClass) {
          return DropdownMenuItem<int>(
            value: int.parse(currentClass['id']),
            child: Text(currentClass['name']),
          );
        }).toList(),
      )
    ]);

    showDialog(
        context: context,
        child: AlertDialog(
          content: Column(
            children: <Widget>[
              TextField(
                controller: _testTimeController,
                decoration:
                    InputDecoration(labelText: S.of(context).testTimeInMinutes),
                keyboardType: TextInputType.number,
              ),
              TextField(
                controller: _testExpiryController,
                decoration: InputDecoration(
                    labelText: S.of(context).allowStartingTestingForInMinutes),
                keyboardType: TextInputType.number,
              ),
              classesDropdown,
            ],
          ),
          actions: <Widget>[
            MaterialButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).cancel),
            ),
            MaterialButton(
              onPressed: () {
                setState(() {
                  _loadWriteTest = false;
                });
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (b) => WriteJoinTestDialog(
                        id: widget.id,
                        time: int.parse(_testTimeController.text),
                        expiry: int.parse(_testExpiryController.text),
                        testClass: _selectedClass)));
              },
              child: Text(S.of(context).startTest),
            )
          ],
        ));
  }
}

class WriteJoinTestDialog extends StatefulWidget {
  final int id, time, expiry, testClass;

  WriteJoinTestDialog(
      {@required this.id,
      @required this.testClass,
      @required this.time,
      @required this.expiry});

  @override
  _WriteJoinTestDialogState createState() => _WriteJoinTestDialogState();
}

class _WriteJoinTestDialogState extends State<WriteJoinTestDialog> {
  bool _testActivated = false;
  Map _testData;

  bool _timerStarted = false;
  Widget _timer;

  @override
  void initState() {
    globals.api.call('activateJoinTest', {
      'id': widget.id,
      'class': widget.testClass,
      'time': widget.time,
      'expiry': widget.expiry
    }).then((data) {
      setState(() {
        _testData = data['response'];
        _testActivated = true;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (ResponsiveDrawerScaffold(
      title: S.of(context).writeJoinTest,
      appBarLeading: IconButton(
        icon: Icon(Icons.close),
        onPressed: () => Navigator.of(context)
            .push(MaterialPageRoute(builder: (b) => JoinTests())),
        tooltip: S.of(context).close,
      ),
      body: (_testActivated)
          ? Column(
              children: <Widget>[
                TestAppCard(
                  children: <Widget>[
                    Text(
                      S.of(context).info,
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    ListTile(
                      title: Text(S.of(context).processingTime +
                          ": ${(int.parse(_testData['writingTime'].toString()) / 60).toString()} " +
                          S.of(context).minutes),
                      leading: Icon(FontAwesomeIcons.stopwatch),
                      subtitle: Wrap(
                        children: (_timerStarted)
                            ? [
                                Text(S.of(context).timeLeft + ': '),
                                _timer,
                              ]
                            : [
                                OutlineButton(
                                  onPressed: () {
                                    setState(() {
                                      _timerStarted = true;
                                      _timer = JoinTestTimer(
                                        time: int.parse(_testData['writingTime']
                                            .toString()),
                                        color: Colors.black,
                                      );
                                    });
                                  },
                                  child: Text(S.of(context).startPersonalTimer),
                                )
                              ],
                      ),
                    ),
                    Divider(),
                    ListTile(
                      title: Text(S.of(context).joinId + ": "),
                      leading: Icon(FontAwesomeIcons.hashtag),
                      isThreeLine: true,
                      subtitle: Center(
                        child: Text(
                          _testData['id'].toString(),
                          style: Theme.of(context).textTheme.headline1,
                        ),
                      ),
                    ),
                    Text(S.of(context).pleaseShareThisIdToYourStudents),
                    Divider(),
                    ListTile(
                      title: OutlineButton(
                          child: Text(
                              S.of(context).finishedWritingTakeMeToClassScore),
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (b) =>
                                    ClassScore(classId: widget.testClass)));
                          }),
                      leading: Icon(FontAwesomeIcons.users),
                    )
                  ],
                )
              ],
            )
          : CenterProgress(
              label: S.of(context).activatingTest,
            ),
    ));
  }
}
