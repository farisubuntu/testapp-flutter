import 'package:flutter/material.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/style.dart';
import 'package:test_app_flutter/writeTest.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class JoinTest extends StatefulWidget {
  JoinTest({Key key, exercises}) : super(key: key);

  @override
  _JoinTestState createState() => _JoinTestState();
}

class _JoinTestState extends State<JoinTest> {
  var api = globals.api;
  int _joinId;
  bool _showSnackBar = false;

  void _startTest() {
    Map<String, dynamic> options = Map();
    options['id'] = _joinId;
    options['images'] = true;
    api.call('joinTest', options).then(((data) {
      if (data['response'] == false) {
        setState(() {
          _showSnackBar = true;
        });
      } else {
        var time = data['response']['joinTestTime'];
        data['response'].remove('joinTestTime');
        //Converting map to list
        List exercises = List();
        data['response'].keys.toList().forEach((key) {
          exercises.add(data['response'][key]);
        });
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => WriteTest(
                  exercises: exercises,
                  joinId: _joinId,
                  testTime: int.parse(time))),
        );
      }
    }));
    setState(() {
      _showSnackBar = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveDrawerScaffold(
      body: Column(
        children: <Widget>[
          TestAppCard(
            children: <Widget>[
              Text(
                S.of(context).joinTest,
                style: Theme.of(context).textTheme.headline6,
              ),
              TextField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    labelText: S.of(context).joinid,
                    helperText:
                        S.of(context).ifYouDontHaveAJoinidPleaseAskYourTeacher),
                onChanged: (newValue) {
                  setState(() {
                    _joinId = int.parse(newValue);
                  });
                },
              ),
              RaisedButton(
                child: Text(S.of(context).startTest),
                onPressed: () => _startTest(),
              ),
              if (_showSnackBar)
                Text(
                  S.of(context).ooopsThisJoinidIsNotValidForYou,
                  style: TextStyle(color: Colors.deepOrange[300]),
                ),
            ],
          ),
        ],
      ),
    );
  }
}
