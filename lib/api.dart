import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_app_flutter/login.dart';


class TestAppI {
  String baseUrl = 'https://testapp.ga/api/';
  String _sid = ''; // Session ID
  Future _doneFuture;

  TestAppI() {
    _doneFuture = _init();
  }

  Future get initializationDone => _doneFuture;

  Future<Map<String, dynamic>> call(String command,
      [Map<String, dynamic> options]) async {
    if (options == null) options = Map();

    options["job"] = command;

    Map<String, String> headers = {};
    if (_sid != '' && _sid != null) {
      options["sid"] = _sid;
      if (!kIsWeb) headers['Cookie'] = 'PHPSESSID=$_sid';
    }

    String body = json.encode(options);

    //print(body);

    final response = await http.post(baseUrl, headers: headers, body: body);

    if (response.statusCode == 200) {
      var res = json.decode(response.body);
      if (res["errors"].contains('NO_SESSION')) {
        LoginPage();
      }
      //print(res);
      return (res);
    } else {
      throw Exception('HTTP ERROR!');
    }
  }

  Future<bool> _init() async {
    try {
      _sid = await Preferences().fetch("sid");
    } catch (e) {
      _saveSID();
    }
    if (_sid == null || _sid == '') return (await _saveSID());
    return (true);
  }

  Future<bool> _saveSID() async {
    return (call('loginState').then((body) {
      this._sid = body['sid'].toString();
      Preferences().save("sid", _sid);
      return (true);
    }));
  }

  void setBaseUrl(String newBase) {
    newBase = newBase.trim().toLowerCase();
    if (newBase.substring(0, 7) == 'http://') newBase = newBase.substring(7);
    if (newBase.substring(0, 8) != 'https://') {
      newBase.replaceFirst('^\\w+:', '');
      newBase = 'https://' + newBase;
    }
    if (newBase.substring(newBase.length - 1) != '/') newBase = newBase + '/';
    if (newBase.substring(newBase.length - 4) != 'api/')
      newBase = newBase + 'api/';
    baseUrl = newBase;
  }
}

class Preferences {
  Future<bool> save(key, value) async {
    //SharedPreferences.setMockInitialValues({});
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(key, value);
  }

  Future<String> fetch(key) async {
    //SharedPreferences.setMockInitialValues({});
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }
}
