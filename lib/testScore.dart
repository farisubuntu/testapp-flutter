import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/chart.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/style.dart';
import 'package:test_app_flutter/userScore.dart';
import 'package:test_app_flutter/writeTest.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class TestScore extends StatefulWidget {
  final int testId;
  final bool backToUserScore;
  final bool hideEasterEgg;

  TestScore(
      {Key key,
      @required this.testId,
      this.backToUserScore = false,
      this.hideEasterEgg = false})
      : super(key: key);

  @override
  _TestScoreState createState() => _TestScoreState();
}

class _TestScoreState extends State<TestScore> {
  Map _testScore = Map();
  bool _testFetched = false;

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> options = Map();
    options['id'] = widget.testId;
    if (!_testFetched)
      globals.api.call("testScore", options).then((response) {
        if (response['response'] is Map) {
          _testScore = response['response'];
        }
        setState(() {
          _testFetched = true;
        });
      });
    return (ResponsiveDrawerScaffold(
        title: S.of(context).testScore,
        appBarLeading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => (widget.backToUserScore)
              ? Navigator.of(context)
                  .push(MaterialPageRoute(builder: (b) => UserScore()))
              : Navigator.of(context).pop(),
          tooltip: S.of(context).close,
        ),
        body: (_testFetched)
            ? (_testScore.isNotEmpty)
                ? Container(
                    color: Colors.white,
                    child: ListView.builder(
                        itemCount: _testScore['answers'].length + 1,
                        itemBuilder: (context, index) {
                          if (index == 0) {
                            return Column(
                              children: <Widget>[
                                ScoreChart(
                                  100 * _testScore['score'].toDouble(),
                                  labelText: (widget.hideEasterEgg)
                                      ? S.of(context).studentGenitive
                                      : S.of(context).your,
                                ),
                                (!widget.hideEasterEgg)
                                    ? RandomBear(
                                        type: (_testScore['score'].toDouble() <
                                                0.65)
                                            ? 'bad'
                                            : (_testScore['score'].toDouble() ==
                                                        1.0 &&
                                                    new Random().nextInt(10) ==
                                                        9)
                                                ? 'super'
                                                : 'good',
                                        scale: 1.5,
                                      )
                                    : Container(),
                              ],
                            );
                          }
                          var currentAnswer = _testScore['answers'][index - 1];

                          List<Widget> yourAnswerRow = [
                            Text((widget.hideEasterEgg)
                                ? S.of(context).studentsAnswer
                                : S.of(context).yourAnswer)
                          ];
                          List<Widget> correctAnswerRow = [
                            Text(S.of(context).correctAnswer)
                          ];
                          jsonDecode(currentAnswer['answer'])
                              .forEach(((awText) {
                            if (awText != null)
                              yourAnswerRow.add(Chip(
                                label: TestAppTex(
                                  awText,
                                  inheritWidth: false,
                                  background: Colors.grey.shade300,
                                ),
                              ));
                          }));
                          jsonDecode(currentAnswer['correct'])
                              .forEach(((awText) {
                            if (awText != null)
                              correctAnswerRow.add(Chip(
                                label: TestAppTex(awText,
                                    inheritWidth: false,
                                    background: Colors.grey.shade300),
                              ));
                          }));

                          return (ExpansionTile(
                            title: TestAppTex(
                              currentAnswer['question'],
                              inheritWidth: false,
                            ),
                            leading: (currentAnswer['match'])
                                ? Icon(FontAwesomeIcons.checkCircle)
                                : Icon(FontAwesomeIcons.timesCircle),
                            backgroundColor: (currentAnswer['match'])
                                ? Colors.lightGreen[300]
                                : Colors.deepOrange[300],
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: IntrinsicHeight(
                                  child: Container(
                                    constraints: BoxConstraints.expand(),
                                    child: Wrap(
                                      direction: Axis.horizontal,
                                      alignment: WrapAlignment.start,
                                      crossAxisAlignment:
                                          WrapCrossAlignment.center,
                                      spacing: 4,
                                      runSpacing: 4,
                                      children: yourAnswerRow,
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: IntrinsicHeight(
                                  child: Container(
                                    constraints: BoxConstraints.expand(),
                                    child: Wrap(
                                      direction: Axis.horizontal,
                                      alignment: WrapAlignment.start,
                                      crossAxisAlignment:
                                          WrapCrossAlignment.center,
                                      spacing: 4,
                                      runSpacing: 4,
                                      children: correctAnswerRow,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ));
                        }),
                  )
                : Column(
                    children: <Widget>[
                      TestAppCard(
                        children: [
                          Text(
                            S.of(context).noDataAvailable,
                            style: Theme.of(context).textTheme.headline6,
                          ),
                          ListTile(
                              leading: Icon(Icons.info),
                              title: Text(S
                                  .of(context)
                                  .errorFetchingYourScoreMaybeTheTestSumbissionWasBroken),
                              trailing: OutlineButton(
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (c) => UserScore()));
                                },
                                child: Text(S.of(context).goBack),
                              ))
                        ],
                      ),
                    ],
                  )
            : CenterProgress()));
  }
}
