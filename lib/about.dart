import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/gdpr.dart';
import 'package:test_app_flutter/style.dart';
import 'package:url_launcher/url_launcher.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class About extends StatefulWidget {
  About({Key key}) : super(key: key);

  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  var api = globals.api;

  @override
  Widget build(BuildContext context) {
    List platforms = [
      {
        'icon': FontAwesomeIcons.android,
        'name': 'Android',
        'link':
            'http://play.google.com/store/apps/details?id=ga.testapp.testapp',
      },
      {
        'icon': FontAwesomeIcons.windows,
        'name': 'Windows 10',
        'link': 'https://www.microsoft.com/store/productId/9P9JJFMFF77D',
      },
      {
        'icon': FontAwesomeIcons.chrome,
        'name': 'Chrome',
        'link':
            'https://chrome.google.com/webstore/detail/testapp-system/hclopnbfffconajgdcmibjekjhmfegjf',
      },
      {
        'icon': FontAwesomeIcons.linux,
        'name': 'Linux',
        'link': 'https://snapcraft.io/testapp-desktop',
      },
      {
        'icon': FontAwesomeIcons.apple,
        'name': 'iOS',
        'link': 'https://apps.apple.com/us/app/testapp-system/id1490425513',
      },
    ];
    return ResponsiveDrawerScaffold(
        title: S.of(context).aboutTestapp,
        body: ListView(children: [
          TestAppCard(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  S.of(context).about,
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(S
                    .of(context)
                    .testappIsAnEducationalOpensourceProjectForWritingClassTests),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  S
                      .of(context)
                      .useOfTestappIsFreeForOrganizationsConditionsApplyntheSource,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: RichText(
                    text: TextSpan(
                  style: TextStyle(color: Colors.black),
                  children: [
                    TextSpan(
                      text: S.of(context).chaw,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    TextSpan(
                        text: S
                            .of(context)
                            .niqhomLoJiqochbeQuvJilijJijatlhmoTlhinganHol)
                  ],
                )),
              ),
              ButtonBar(
                children: <Widget>[
                  OutlineButton.icon(
                    label: Text('GitLab'),
                    icon: Icon(FontAwesomeIcons.gitlab),
                    onPressed: () {
                      launch('https://gitlab.com/testapp-system/');
                    },
                  ),
                  OutlineButton.icon(
                    label: Text('EUPL-1.2'),
                    icon: Icon(FontAwesomeIcons.balanceScaleRight),
                    onPressed: () {
                      launch(S.of(context).euplUrl);
                    },
                  )
                ],
              )
            ],
          ),
          TestAppCard(
            children: <Widget>[
              Text(
                S.of(context).testappForAllDevices,
                style: Theme.of(context).textTheme.headline6,
              ),
              ListView.separated(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: platforms.length,
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(),
                  itemBuilder: (context, index) {
                    return (ListTile(
                      onTap: () {
                        launch(platforms[index]['link']);
                      },
                      leading: Icon(platforms[index]['icon']),
                      title: Text(platforms[index]['name']),
                    ));
                  })
            ],
          ),
          TestAppCard(
            children: <Widget>[
              Text(S.of(context).donate,
                  style: Theme.of(context).textTheme.headline6),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(S.of(context).donateToTestappOpenSourceProject),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: RichText(
                  text: TextSpan(
                      style: TextStyle(color: Colors.black),
                      children: [
                        TextSpan(
                          text: S.of(context).helpUs,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        TextSpan(
                            text: S
                                .of(context)
                                .forPublishingThisAppOnAndroidAndWindowsWeNeed),
                        TextSpan(
                          text: '200\$',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ]),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.dollarSign),
                    RaisedButton.icon(
                        onPressed: () =>
                            launch('https://buymeacoff.ee/JasMich'),
                        icon: Icon(FontAwesomeIcons.coffee),
                        label: Text(S.of(context).viewTestappOnBuyMeACoffee))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.bitcoin),
                    SelectableText(
                      ' 3NUiJXDCkyRTb9Tg7n63yK6Y7CexADtSEh',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    )
                  ],
                ),
              )
            ],
          ),
          TestAppCard(children: [
            Text(
              S.of(context).legalNoticeAndPrivacyPolicy,
              style: Theme.of(context).textTheme.headline6,
            ),
            Text(S
                .of(context)
                .asThisApplicationIsANoncommercialStudentProjectByA),
            GDPR()
          ]),
        ]));
  }
}
