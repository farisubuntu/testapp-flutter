import 'package:flutter/material.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/editJoinTest.dart';
import 'package:test_app_flutter/style.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class JoinTests extends StatefulWidget {
  @override
  _JoinTestsState createState() => _JoinTestsState();
}

class _JoinTestsState extends State<JoinTests> {
  bool _loadedTests = false;
  List _joinTests;

  @override
  void initState() {
    super.initState();
    globals.api.call('listJoin', {'showAll': true}).then((data) {
      if (data['response'] is List) {
        _joinTests = data['response'].reversed.toList();
      } else
        _joinTests = [];
      setState(() {
        _loadedTests = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return (ResponsiveDrawerScaffold(
      body: Column(
        children: <Widget>[
          TestAppCard(
            children: <Widget>[
              Text(
                S.of(context).joinTests,
                style: Theme.of(context).textTheme.headline6,
              ),
              (_loadedTests)
                  ? (_joinTests.isNotEmpty)
                      ? ListView.separated(
                          shrinkWrap: true,
                          itemBuilder: (c, index) {
                            return (ListTile(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => EditJoinTest(
                                          id: int.parse(
                                              _joinTests[index]['id']),
                                          testData: _joinTests[index],
                                        )));
                              },
                              title: Text(_joinTests[index]['name']),
                              trailing: Text(timeago.format(
                                  DateTime.fromMicrosecondsSinceEpoch(int.parse(
                                      _joinTests[index]['timestamp'] +
                                          '000000')))),
                            ));
                          },
                          separatorBuilder: (c, index) => Divider(),
                          itemCount: _joinTests.length)
                      : ListTile(
                          leading: Icon(Icons.info),
                          title: Text(S.of(context).thereAreNoJoinTestsYet),
                        )
                  : CenterProgress()
            ],
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: S.of(context).createNewJoinTest,
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (b) => EditJoinTest()));
        },
      ),
    ));
  }
}
