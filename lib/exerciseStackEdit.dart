import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/editExercise.dart';
import 'package:test_app_flutter/style.dart';
import 'package:url_launcher/url_launcher.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class ExerciseStackEdit extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _EditExerciseStackEditState();
}

class _EditExerciseStackEditState extends State<ExerciseStackEdit> {
  TextEditingController _exerciseTitleController = TextEditingController();
  List<List<Map<String, TextEditingController>>> exercises = [];
  Set topics;
  bool _generateBothWay = true;
  bool _loading = false;
  double _generationPercentage = 0;

  @override
  void initState() {
    _addExercise();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (ResponsiveDrawerScaffold(
        title: S.of(context).exerciseStackEdit,
        floatingActionButton: (!_loading)
            ? FloatingActionButton.extended(
                onPressed: generateExercises,
                label: Text(S.of(context).generateExercises),
                icon: Icon(Icons.check),
              )
            : FloatingActionButton(
                onPressed: () {
                  setState(() {
                    _loading = !_loading;
                  });
                },
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Colors.white),
                ),
              ),
        body: (!_loading)
            ? ListView.separated(
                itemBuilder: (context, index) {
                  if (index == 0)
                    return TestAppCard(children: [
                      Text(
                        S.of(context).exerciseName,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      TextField(
                        controller: _exerciseTitleController,
                        decoration: InputDecoration(
                            labelText: S.of(context).exercisesNameGenitiv,
                            helperText: S
                                .of(context)
                                .willBeAppliedToAllGeneratedExercises),
                      ),
                      Container(
                        height: 16,
                      ),
                      Text(
                        S.of(context).topics,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      TopicSelector(
                          onSelect: (newTopics) => topics = newTopics),
                      Container(
                        height: 16,
                      ),
                      Text(
                        S.of(context).options,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      SwitchListTile(
                          title: Text(S.of(context).generateInBothWays),
                          subtitle: Text(S
                              .of(context)
                              .unselectThisIfYouOnlyWantToCreateExercisesFrom),
                          value: _generateBothWay,
                          onChanged: (state) => setState(() {
                                _generateBothWay = state;
                              })),
                      Container(
                        height: 16,
                      ),
                      Text(S.of(context).copyright,
                          style: Theme.of(context).textTheme.headline6),
                      ListTile(
                        onTap: () => launch(S.of(context).creativeCommonsUrl),
                        trailing: SizedBox(
                          width: 72,
                          child: Row(
                            children: <Widget>[
                              Icon(FontAwesomeIcons.creativeCommons),
                              Icon(FontAwesomeIcons.creativeCommonsBy),
                              Icon(FontAwesomeIcons.creativeCommonsSa),
                            ],
                          ),
                        ),
                        title:
                            Text(S.of(context).iOwnTheCopyrightOfThisExercise),
                        subtitle: Text(S
                            .of(context)
                            .bySavingThisExerciseIAsignAllRightsOfThis),
                      )
                    ]);
                  if (index == exercises.length + 1) {
                    return ListTile(
                      title: OutlineButton(
                        onPressed: _addExercise,
                        child: Text(S.of(context).addOneMore),
                      ),
                    );
                  }
                  return (ListTile(
                      title: Wrap(spacing: 18, runSpacing: 16, children: [
                    Row(
                      children: <Widget>[
                        Text(
                          S.of(context).exercise + ' $index',
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ],
                    ),
                    Container(
                      constraints: BoxConstraints(maxWidth: 256),
                      child: TextField(
                        onChanged: _editingComplete,
                        controller: exercises[index - 1][0]['text'],
                        decoration: InputDecoration(
                            labelText: S.of(context).language1,
                            helperText: S.of(context).humanreadableVersion),
                      ),
                    ),
                    Container(
                      constraints: BoxConstraints(maxWidth: 256),
                      child: TextField(
                        controller: exercises[index - 1][0]['regex'],
                        decoration: InputDecoration(
                            labelText: S.of(context).language1Regex,
                            helperText: S.of(context).noDelimiterRequired),
                      ),
                    ),
                    Container(
                      constraints: BoxConstraints(maxWidth: 256),
                      child: TextField(
                        controller: exercises[index - 1][1]['text'],
                        decoration: InputDecoration(
                            labelText: S.of(context).language2,
                            helperText: S.of(context).humanreadableVersion),
                      ),
                    ),
                    Container(
                      constraints: BoxConstraints(maxWidth: 256),
                      child: TextField(
                        controller: exercises[index - 1][1]['regex'],
                        decoration: InputDecoration(
                            labelText: S.of(context).language2Regex,
                            helperText: S.of(context).noDelimiterRequired),
                      ),
                    ),
                  ])));
                },
                separatorBuilder: (i, c) => Divider(),
                itemCount: exercises.length + 2)
            : Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircularProgressIndicator(
                        value: _generationPercentage,
                      ),
                      Text('${(_generationPercentage * 100).round()}% ' +
                          S.of(context).generated +
                          '...')
                    ],
                  ),
                ),
              )));
  }

  void _editingComplete(val) {
    if (!_hasContentAtIndex(exercises.length))
      setState(() {
        _removeEmptyExercises();
      });
  }

  bool _hasContentAtIndex(int i) {
    var controllers = exercises[i];
    return (controllers[0]['text'].text.isNotEmpty &&
        controllers[1]['text'].text.isNotEmpty);
  }

  void _removeEmptyExercises() {
    for (int i = 0; i < exercises.length; i++) {
      if (!_hasContentAtIndex(i)) exercises.removeAt(i);
    }
  }

  void _addExercise() {
    _removeEmptyExercises();
    setState(() {
      exercises.add([
        {'text': TextEditingController(), 'regex': TextEditingController()},
        {'text': TextEditingController(), 'regex': TextEditingController()}
      ]);
    });
  }

  void generateExercises() async {
    // Just a pretty comfort feature
    int totalExercises = exercises.length;
    if (_generateBothWay) totalExercises *= 2;
    totalExercises += 1; // We don't want to rest at 100% for 5 minutes...

    setState(() {
      _loading = true;
      _generationPercentage = 0;
    });
    _removeEmptyExercises();

    // Building all options-Maps for the generation
    int startingLanguage = 0; // "Language 1" first
    while (startingLanguage < ((_generateBothWay) ? 2 : 1)) {
      var questionLanguage = startingLanguage;
      var answerLanguage = (startingLanguage == 0) ? 1 : 0;
      for (int i = 0; i < exercises.length; i++) {
        var exerciseTemplate = exercises[i];
        Map<String, dynamic> options = Map();

        options['name'] = _exerciseTitleController.text.trim();
        options['content'] =
            exerciseTemplate[questionLanguage]['text'].text.trim();

        if (exerciseTemplate[answerLanguage]['regex'].text.trim().isEmpty) {
          options['type'] = 'inputMultiple';

          options['answers'] = [
            exerciseTemplate[answerLanguage]['text'].text.trim()
          ];
          options['correct'] = [
            exerciseTemplate[answerLanguage]['text'].text.trim()
          ];
        } else {
          options['type'] = 'multipleRegEx';

          options['answers'] = [
            exerciseTemplate[answerLanguage]['regex'].text.trim()
          ];
          options['correct'] = [
            exerciseTemplate[answerLanguage]['text'].text.trim()
          ];
        }

        // Adding topics
        options['topics'] = topics.toList();

        if (!(topics.isNotEmpty &&
            options['name'].isNotEmpty &&
            options['content'].isNotEmpty &&
            options['answers'].isNotEmpty &&
            options['correct'].isNotEmpty)) {
          _loading = false;
          return;
        }

        await globals.api.call('addExercise', options);
        setState(() {
          _generationPercentage =
              i / totalExercises + ((startingLanguage == 0) ? 0 : 0.5);
        });
      }
      startingLanguage++;
    }
    Navigator.of(context).pop();
  }
}
