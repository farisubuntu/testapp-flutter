import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:share/share.dart';
import 'package:test_app_flutter/chart.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/randomTest.dart';
import 'package:test_app_flutter/style.dart';
import 'package:test_app_flutter/testScore.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class UserScore extends StatefulWidget {
  UserScore({
    Key key,
  }) : super(key: key);

  @override
  _UserScoreState createState() => _UserScoreState();
}

class _UserScoreState extends State<UserScore> {
  int _fetched = 0;
  double _userScore = -1;
  List _tests = [];
  Map _subjectScores = Map();

  bool _noScoreAvailable = false;

  @override
  void initState() {
    globals.api.call("userScore").then((response) {
      setState(() {
        _fetched++;
        if (!(response['response'] is bool)) {
          _userScore = response['response'];
        } else
          _noScoreAvailable = true;
      });
    });
    globals.api.call("listTests").then((response) {
      setState(() {
        _fetched++;
        if (!(response['response'] is bool)) {
          _tests = response['response'];
        } else
          _tests = [];
      });
    });
    globals.api.call("listTopics").then((response) {
      setState(() {
        _fetched++;
      });
      _subjectScores.clear();
      Set subjects = Set();
      response['response'].forEach((topic) => subjects.add(topic['subject']));
      subjects.forEach((subject) => globals.api
              .call("userSubjectScore", {'subject': subject}).then((response) {
            if (!(response['response'] is bool))
              setState(() {
                _subjectScores[subject] = (response['response'] * 100).round();
              });
          }));
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (ResponsiveDrawerScaffold(
        body: (_fetched >= 3)
            ? (!_noScoreAvailable)
                ? ListView.separated(
                    separatorBuilder: (BuildContext context, int index) =>
                        (index != 0) ? const Divider() : Container(),
                    itemCount: _tests.length + 2,
                    itemBuilder: (context, index) {
                      if (index == 0)
                        return TestAppCard(
                            children: [ScoreChart(100 * _userScore)]);
                      if (index == 1) {
                        List<Widget> subjectCards = [];
                        _subjectScores.keys.toList().forEach((subjectName) {
                          var background = colorByText(subjectName);
                          subjectCards.add(Card(
                            color: background,
                            child: SizedBox(
                              width: 200,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    subjectName,
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        .copyWith(color: Colors.white),
                                  ),
                                  Text(
                                    _subjectScores[subjectName].toString() +
                                        "%",
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline2
                                        .copyWith(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ),
                          ));
                        });
                        return SizedBox(
                            height: 150,
                            child: (ListView(
                                scrollDirection: Axis.horizontal,
                                children: subjectCards)));
                      }
                      var currentTest = _tests.reversed.toList()[index - 2];
                      return (TestScoreDetailTile(
                        testId: currentTest['id'],
                        timestamp: currentTest['timestamp'],
                      ));
                    })
                : Column(
                    children: <Widget>[
                      TestAppCard(
                        children: [
                          Text(
                            S.of(context).noScoreAvailable,
                            style: Theme.of(context).textTheme.headline6,
                          ),
                          ListTile(
                              leading: Icon(Icons.info),
                              title:
                                  Text(S.of(context).youDidntWriteAnyTestYet),
                              trailing: OutlineButton(
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (b) => RandomTest()));
                                },
                                child: Text(S.of(context).writeTest),
                              ))
                        ],
                      ),
                    ],
                  )
            : CenterProgress()));
  }
}

class TestScoreDetailTile extends StatefulWidget {
  final String testId;
  final String timestamp; //currentTest['timestamp']

  TestScoreDetailTile({@required this.testId, @required this.timestamp});

  @override
  _TestScoreDetailTileState createState() => _TestScoreDetailTileState();
}

class _TestScoreDetailTileState extends State<TestScoreDetailTile> {
  bool _detailsLoaded = false;
  double _testScore;
  DateTime _time;

  void load() async {
    Map<String, dynamic> options = Map();
    options['id'] = widget.testId;
    globals.api.call("testScore", options).then((response) {
      if (!(response['response'] is bool)) {
        _testScore = response['response']['score'].toDouble();
      } else {
        _testScore = -1.0;
      }
      setState(() {
        _detailsLoaded = true;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _time = new DateTime.fromMicrosecondsSinceEpoch(
        int.parse(widget.timestamp + '000000'));
  }

  @override
  Widget build(BuildContext context) {
    return (ExpansionTile(
      onExpansionChanged: (opened) {
        if (opened && !_detailsLoaded) load();
      },
      leading: Text(widget.testId),
      title: Text(timeago.format(_time)),
      children: <Widget>[
        (_detailsLoaded)
            ? TestScoreDetail(
                score: _testScore,
                testId: widget.testId,
                timestamp: _time,
              )
            : CenterProgress()
      ],
    ));
  }
}

class TestScoreDetail extends StatelessWidget {
  final double score;
  final String testId;
  final DateTime timestamp;

  TestScoreDetail(
      {@required this.score, @required this.testId, @required this.timestamp});

  @override
  Widget build(BuildContext context) {
    return TestAppCard(children: [
      ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          ListTile(
            leading: Icon(FontAwesomeIcons.hashtag),
            title: Text(testId.toString()),
          ),
          (score != -1.0)
              ? ScoreChart(score * 100)
              : Center(child: Text(S.of(context).noScoreAvailable)),
          ListTile(
            leading: Icon(FontAwesomeIcons.info),
            title: RaisedButton(
              child: Text(S.of(context).moreDetails),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => TestScore(
                            testId: int.parse(testId),
                          )),
                );
              },
            ),
          ),
          ListTile(
            leading: Icon(Icons.share),
            title: OutlineButton(
                onPressed: () {
                  Share.share(S.of(context).myTestScoreOnTestappFrom +
                      """+${timeago.format(timestamp)}:

${(score * 100).round()}%""");
                },
                child: Text(S.of(context).shareThisScore)),
          ),
        ],
      )
    ]);
  }
}
