import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/about.dart';
import 'package:test_app_flutter/api.dart';
import 'package:test_app_flutter/classes.dart';
import 'package:test_app_flutter/exercises.dart';
import 'package:test_app_flutter/joinTest.dart';
import 'package:test_app_flutter/joinTests.dart';
import 'package:test_app_flutter/randomTest.dart';
import 'package:test_app_flutter/settings.dart';
import 'package:test_app_flutter/userScore.dart';

import 'dashboard.dart';
import 'generated/l10n.dart';
import 'globals.dart' as globals;
import 'schoolManagement.dart';

class TestAppDrawer extends StatefulWidget {
  const TestAppDrawer({Key key}) : super(key: key);

  @override
  _TestAppDrawerState createState() => _TestAppDrawerState();
}

class _TestAppDrawerState extends State<TestAppDrawer> {
  bool _isAdmin = false;

  @override
  void initState() {
    globals.api.initializationDone.then(checkAdmin);
    super.initState();
  }

  void checkAdmin(param) {
    Preferences().fetch('Admin').then((adminString) {
      setState(() {
        _isAdmin = adminString == 'true';
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Container(
      color: Colors.white,
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          Semantics(
            hidden: true,
            child: UserAccountsDrawerHeader(
              accountName: Text(
                "TestApp",
                style: Theme.of(context).textTheme.headline3,
              ),
              accountEmail: Text(S.of(context).slogan),
              currentAccountPicture: Image.asset("assets/bear-standard.png"),
              decoration: BoxDecoration(color: Colors.lightBlue),
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text(S.of(context).dashboard),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Dashboard(
                          title: "TestApp",
                        )),
              );
            },
          ),
          ListTile(
            leading: Icon(FontAwesomeIcons.penFancy),
            title: Text(S.of(context).randomTest),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => RandomTest()),
              );
            },
          ),
          ListTile(
            leading: Icon(FontAwesomeIcons.chalkboardTeacher),
            title: Text(S.of(context).joinTest),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => JoinTest()),
              );
            },
          ),
          ListTile(
            leading: Icon(FontAwesomeIcons.chartLine),
            title: Text(S.of(context).yourScore),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => UserScore()),
              );
            },
          ),
          if (_isAdmin) Divider(),
          if (_isAdmin)
            ListTile(
              leading: Icon(FontAwesomeIcons.graduationCap),
              title: Text(S.of(context).exercises),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Exercises()),
                );
              },
            ),
          if (_isAdmin)
            ListTile(
              leading: Icon(FontAwesomeIcons.users),
              title: Text(S.of(context).classes),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Classes()),
                );
              },
            ),
          if (_isAdmin)
            ListTile(
              leading: Icon(FontAwesomeIcons.kiwiBird),
              title: Text(S.of(context).joinTests),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => JoinTests()),
                );
              },
            ),
          if (_isAdmin)
            ListTile(
              leading: Icon(FontAwesomeIcons.school),
              title: Text(S.of(context).schoolManagement),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SchoolManagement()),
                );
              },
            ),
          Divider(),
          ListTile(
            leading: Icon(FontAwesomeIcons.info),
            title: Text(S.of(context).about),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => About()),
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text(S.of(context).settings),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SettingsPage()),
              );
            },
          ),
        ],
      ),
    ));
  }
}

class ResponsiveDrawerScaffold extends StatefulWidget {
  final Widget body;
  final String title;
  final Widget appBarLeading;
  final List<Widget> appBarActions;
  final Widget appBarBottom;
  final FloatingActionButtonLocation floatingActionButtonLocation;
  final FloatingActionButton floatingActionButton;
  final bool showDrawer;

  ResponsiveDrawerScaffold(
      {this.body,
      this.title = 'TestApp',
      this.appBarLeading,
      this.appBarActions,
      this.appBarBottom,
      this.floatingActionButton,
      this.floatingActionButtonLocation,
      this.showDrawer = true}) {
    assert(title != '');
  }

  @override
  _ResponsiveDrawerScaffoldState createState() =>
      _ResponsiveDrawerScaffoldState();
}

class _ResponsiveDrawerScaffoldState extends State<ResponsiveDrawerScaffold> {
  @override
  Widget build(BuildContext context) {
    if (MediaQuery.of(context).size.width < 1024) {
      return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          actions: widget.appBarActions,
          bottom: widget.appBarBottom,
          leading: widget.appBarLeading,
        ),
        drawer: (widget.showDrawer) ? TestAppDrawer() : null,
        body: widget.body,
        floatingActionButton: widget.floatingActionButton,
        floatingActionButtonLocation: widget.floatingActionButtonLocation,
      );
    } else {
      return Row(children: [
        if (widget.showDrawer) Container(width: 256, child: TestAppDrawer()),
        Expanded(
          child: Scaffold(
            appBar: AppBar(
              title: Text(widget.title),
              actions: widget.appBarActions,
              bottom: widget.appBarBottom,
              automaticallyImplyLeading: false,
              leading: widget.appBarLeading,
            ),
            body: widget.body,
            floatingActionButton: widget.floatingActionButton,
            floatingActionButtonLocation: widget.floatingActionButtonLocation,
          ),
        )
      ]);
    }
  }
}
