// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

class S {
  S(this.localeName);
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final String name = locale.countryCode.isEmpty ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return S(localeName);
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  final String localeName;

  String get slogan {
    return Intl.message(
      'Learn better',
      name: 'slogan',
      desc: '',
      args: [],
    );
  }

  String get randomTest {
    return Intl.message(
      'Random Test',
      name: 'randomTest',
      desc: '',
      args: [],
    );
  }

  String get joinTest {
    return Intl.message(
      'Join Test',
      name: 'joinTest',
      desc: '',
      args: [],
    );
  }

  String get yourScore {
    return Intl.message(
      'Your Score',
      name: 'yourScore',
      desc: '',
      args: [],
    );
  }

  String get exercises {
    return Intl.message(
      'Exercises',
      name: 'exercises',
      desc: '',
      args: [],
    );
  }

  String get classes {
    return Intl.message(
      'Classes',
      name: 'classes',
      desc: '',
      args: [],
    );
  }

  String get joinTests {
    return Intl.message(
      'Join Tests',
      name: 'joinTests',
      desc: '',
      args: [],
    );
  }

  String get schoolManagement {
    return Intl.message(
      'School management',
      name: 'schoolManagement',
      desc: '',
      args: [],
    );
  }

  String get about {
    return Intl.message(
      'About',
      name: 'about',
      desc: '',
      args: [],
    );
  }

  String get settings {
    return Intl.message(
      'Settings',
      name: 'settings',
      desc: '',
      args: [],
    );
  }

  String get saveTest {
    return Intl.message(
      'Save Test',
      name: 'saveTest',
      desc: '',
      args: [],
    );
  }

  String get submitTest {
    return Intl.message(
      'Submit Test',
      name: 'submitTest',
      desc: '',
      args: [],
    );
  }

  String get goodLuck {
    return Intl.message(
      'Good luck!',
      name: 'goodLuck',
      desc: '',
      args: [],
    );
  }

  String get savingYourTest {
    return Intl.message(
      'Saving your test...',
      name: 'savingYourTest',
      desc: '',
      args: [],
    );
  }

  String get doYouRealyWantToAbortThisTestYourAnswers {
    return Intl.message(
      'Do you realy want to abort this test? Your answers will be lost.',
      name: 'doYouRealyWantToAbortThisTestYourAnswers',
      desc: '',
      args: [],
    );
  }

  String get doYouRealyWantToAbortThisJoinTestIt {
    return Intl.message(
      'Do you realy want to abort this Join Test? It will be automatically submitted with your current answers.',
      name: 'doYouRealyWantToAbortThisJoinTestIt',
      desc: '',
      args: [],
    );
  }

  String get abortTest {
    return Intl.message(
      'Abort Test',
      name: 'abortTest',
      desc: '',
      args: [],
    );
  }

  String get continueWriting {
    return Intl.message(
      'Continue writing',
      name: 'continueWriting',
      desc: '',
      args: [],
    );
  }

  String get answer {
    return Intl.message(
      'Answer',
      name: 'answer',
      desc: '',
      args: [],
    );
  }

  String get lettersNumbersAndTextAllowed {
    return Intl.message(
      'Letters, numbers and text allowed',
      name: 'lettersNumbersAndTextAllowed',
      desc: '',
      args: [],
    );
  }

  String get numbersOnly {
    return Intl.message(
      'Numbers only',
      name: 'numbersOnly',
      desc: '',
      args: [],
    );
  }

  String get orderMatters {
    return Intl.message(
      'Order matters.',
      name: 'orderMatters',
      desc: '',
      args: [],
    );
  }

  String get orderDoesntMatter {
    return Intl.message(
      'Order doesn\'t matter.',
      name: 'orderDoesntMatter',
      desc: '',
      args: [],
    );
  }

  String get noScoreAvailable {
    return Intl.message(
      'No score available',
      name: 'noScoreAvailable',
      desc: '',
      args: [],
    );
  }

  String get youDidntWriteAnyTestYet {
    return Intl.message(
      'You didn\'t write any test yet.',
      name: 'youDidntWriteAnyTestYet',
      desc: '',
      args: [],
    );
  }

  String get writeTest {
    return Intl.message(
      'Write test',
      name: 'writeTest',
      desc: '',
      args: [],
    );
  }

  String get moreDetails {
    return Intl.message(
      'More details',
      name: 'moreDetails',
      desc: '',
      args: [],
    );
  }

  String get shareThisScore {
    return Intl.message(
      'Share this Score',
      name: 'shareThisScore',
      desc: '',
      args: [],
    );
  }

  String get myTestScoreOnTestappFrom {
    return Intl.message(
      'My test Score on TestApp from',
      name: 'myTestScoreOnTestappFrom',
      desc: '',
      args: [],
    );
  }

  String get testScore {
    return Intl.message(
      'Test score',
      name: 'testScore',
      desc: '',
      args: [],
    );
  }

  String get close {
    return Intl.message(
      'Close',
      name: 'close',
      desc: '',
      args: [],
    );
  }

  String get studentGenitive {
    return Intl.message(
      'Student\'s',
      name: 'studentGenitive',
      desc: '',
      args: [],
    );
  }

  String get your {
    return Intl.message(
      'Your',
      name: 'your',
      desc: '',
      args: [],
    );
  }

  String get studentsAnswer {
    return Intl.message(
      'Student\'s answer:',
      name: 'studentsAnswer',
      desc: '',
      args: [],
    );
  }

  String get correctAnswer {
    return Intl.message(
      'Correct answer:',
      name: 'correctAnswer',
      desc: '',
      args: [],
    );
  }

  String get yourAnswer {
    return Intl.message(
      'Your answer:',
      name: 'yourAnswer',
      desc: '',
      args: [],
    );
  }

  String get noDataAvailable {
    return Intl.message(
      'No data available',
      name: 'noDataAvailable',
      desc: '',
      args: [],
    );
  }

  String get errorFetchingYourScoreMaybeTheTestSumbissionWasBroken {
    return Intl.message(
      'Error fetching your score. Maybe the test sumbission was broken.',
      name: 'errorFetchingYourScoreMaybeTheTestSumbissionWasBroken',
      desc: '',
      args: [],
    );
  }

  String get goBack {
    return Intl.message(
      'Go back',
      name: 'goBack',
      desc: '',
      args: [],
    );
  }

  String get loadingData {
    return Intl.message(
      'Loading data...',
      name: 'loadingData',
      desc: '',
      args: [],
    );
  }

  String get account {
    return Intl.message(
      'Account',
      name: 'account',
      desc: '',
      args: [],
    );
  }

  String get logOut {
    return Intl.message(
      'Log out',
      name: 'logOut',
      desc: '',
      args: [],
    );
  }

  String get updatePassword {
    return Intl.message(
      'Update password',
      name: 'updatePassword',
      desc: '',
      args: [],
    );
  }

  String get pleaseProvideANewPasswordWeRecommendChoosingAPassword {
    return Intl.message(
      'Please provide a new password. We recommend choosing a password containing letters, symbols and numbers with at least 8 charcters.',
      name: 'pleaseProvideANewPasswordWeRecommendChoosingAPassword',
      desc: '',
      args: [],
    );
  }

  String get newPassword {
    return Intl.message(
      'New password',
      name: 'newPassword',
      desc: '',
      args: [],
    );
  }

  String get cancel {
    return Intl.message(
      'Cancel',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  String get teacherAccessVoucher {
    return Intl.message(
      'Teacher access voucher',
      name: 'teacherAccessVoucher',
      desc: '',
      args: [],
    );
  }

  String get forRegistrationTeachersNeedAVoucherVouchersCanBeIssued {
    return Intl.message(
      'For registration, teachers need a voucher. Vouchers can be issued by any other teacher and need to be entered during the registration process. Vouchers are valid for one week after their creation.',
      name: 'forRegistrationTeachersNeedAVoucherVouchersCanBeIssued',
      desc: '',
      args: [],
    );
  }

  String get createNewVoucher {
    return Intl.message(
      'Create new voucher',
      name: 'createNewVoucher',
      desc: '',
      args: [],
    );
  }

  String get thereAreNoVouchersYet {
    return Intl.message(
      'There are no vouchers yet.',
      name: 'thereAreNoVouchersYet',
      desc: '',
      args: [],
    );
  }

  String get shortTest5Exercises {
    return Intl.message(
      'Short Test (5 exercises)',
      name: 'shortTest5Exercises',
      desc: '',
      args: [],
    );
  }

  String get smartTest10Exercises {
    return Intl.message(
      'Smart Test (10 exercises)',
      name: 'smartTest10Exercises',
      desc: '',
      args: [],
    );
  }

  String get pleaseSelect {
    return Intl.message(
      'Please select',
      name: 'pleaseSelect',
      desc: '',
      args: [],
    );
  }

  String get startTest {
    return Intl.message(
      'Start test',
      name: 'startTest',
      desc: '',
      args: [],
    );
  }

  String get next {
    return Intl.message(
      'Next',
      name: 'next',
      desc: '',
      args: [],
    );
  }

  String get loadingTest {
    return Intl.message(
      'Loading test...',
      name: 'loadingTest',
      desc: '',
      args: [],
    );
  }

  String get topics {
    return Intl.message(
      'Topics',
      name: 'topics',
      desc: '',
      args: [],
    );
  }

  String get testType {
    return Intl.message(
      'Test Type',
      name: 'testType',
      desc: '',
      args: [],
    );
  }

  String get privacyIsImportantToUs {
    return Intl.message(
      'Datenschutz ist uns wichtig',
      name: 'privacyIsImportantToUs',
      desc: '',
      args: [],
    );
  }

  String get privacyPolicyShort {
    return Intl.message(
      'Dein Score aus Tests ist nur für dich und die Leiter der jeweiligen Kurse sichtbar. Bei jedem Test kannst du ebenfalls auswählen, dass dieser gänzlich privat bleibt. Die ausführliche Beschreibung, wie wir deine Daten behandeln findest du in unserer Datenschutzerklärung.  Es kann dich niemand, weder Eltern oder Freunde, noch Lehrer, zwingen, unsere TestApp und zu benutzen und damit unserer Datenschutzerklärung zuzustimmen.  Falls du Bedenken bezüglich unseres Datenschutzes hast, wende dich gerne an info@testapp.ga. Wenn du dich irgendwann entscheidest, dass du mit unserer Datenverarbeitung nicht mehr einverstanden bist, kannst du jederzeit dein Einverständnis zurückziehen. Weil deine Daten schließlich dir gehoren, kannst du sie auch jederzeit löschen, korrigieren und einsehen. Wir möchten dir den Datenschutz nicht schwer machen und dir deshalb keine komplizierten Formulare vorsetzen. Wenn du von deinen Rechten gebrauch machen willst, kannst du dich deshalb ganz einfach in einer unformalen E-Mail an info@testapp.ga wenden.  Falls du unter 16 Jahre alt bist, bist du dazu verpflichtet, deine Eltern oder Sorgeberechtigen über deine Anmeldung zu informieren und diese müssen unseren Datenschutz-Richtlinien zustimmen.',
      name: 'privacyPolicyShort',
      desc: '',
      args: [],
    );
  }

  String get iReadTheFullPrivacyStatementAndAgreeToAll {
    return Intl.message(
      'I read the full privacy statement and agree to all mentioned points',
      name: 'iReadTheFullPrivacyStatementAndAgreeToAll',
      desc: '',
      args: [],
    );
  }

  String get resetPassword {
    return Intl.message(
      'Reset password',
      name: 'resetPassword',
      desc: '',
      args: [],
    );
  }

  String get areYouSureToResetThePasswordFor {
    return Intl.message(
      'Are you sure to reset the password for',
      name: 'areYouSureToResetThePasswordFor',
      desc: '',
      args: [],
    );
  }

  String get reset {
    return Intl.message(
      'Reset',
      name: 'reset',
      desc: '',
      args: [],
    );
  }

  String get pleaseCheckYourMails {
    return Intl.message(
      'Please check your mails.',
      name: 'pleaseCheckYourMails',
      desc: '',
      args: [],
    );
  }

  String get school {
    return Intl.message(
      'School:',
      name: 'school',
      desc: '',
      args: [],
    );
  }

  String get login {
    return Intl.message(
      'Login',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  String get mailId {
    return Intl.message(
      'Mail ID',
      name: 'mailId',
      desc: '',
      args: [],
    );
  }

  String get password {
    return Intl.message(
      'Password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  String get wrongMailIdOrPassword {
    return Intl.message(
      'Wrong mail id or password.',
      name: 'wrongMailIdOrPassword',
      desc: '',
      args: [],
    );
  }

  String get stayLoggedIn {
    return Intl.message(
      'Stay logged in:',
      name: 'stayLoggedIn',
      desc: '',
      args: [],
    );
  }

  String get register {
    return Intl.message(
      'Register',
      name: 'register',
      desc: '',
      args: [],
    );
  }

  String get thisIsImportantForYourTeachers {
    return Intl.message(
      'This is important for your teachers.',
      name: 'thisIsImportantForYourTeachers',
      desc: '',
      args: [],
    );
  }

  String get fullName {
    return Intl.message(
      'Full name',
      name: 'fullName',
      desc: '',
      args: [],
    );
  }

  String get pleaseProvideAFullNameAValidMailIdAnd {
    return Intl.message(
      'Please provide a full name, a valid mail id and a password.',
      name: 'pleaseProvideAFullNameAValidMailIdAnd',
      desc: '',
      args: [],
    );
  }

  String get teacherRegister {
    return Intl.message(
      'Teacher Register',
      name: 'teacherRegister',
      desc: '',
      args: [],
    );
  }

  String get thisIsImportantForYourStudents {
    return Intl.message(
      'This is important for your students.',
      name: 'thisIsImportantForYourStudents',
      desc: '',
      args: [],
    );
  }

  String get accessVoucher {
    return Intl.message(
      'Access voucher',
      name: 'accessVoucher',
      desc: '',
      args: [],
    );
  }

  String get ifYouDidntGetAVoucherPleaseContactYourSchools {
    return Intl.message(
      'If you didn\'t get a voucher, please contact your school\'s admin.',
      name: 'ifYouDidntGetAVoucherPleaseContactYourSchools',
      desc: '',
      args: [],
    );
  }

  String get connectToOtherServer {
    return Intl.message(
      'Connect to other server...',
      name: 'connectToOtherServer',
      desc: '',
      args: [],
    );
  }

  String get testappServer {
    return Intl.message(
      'TestApp Server',
      name: 'testappServer',
      desc: '',
      args: [],
    );
  }

  String get egMyservercomOrHttpsdevtestappga {
    return Intl.message(
      'E.g. myserver.com or https://dev.testapp.ga/',
      name: 'egMyservercomOrHttpsdevtestappga',
      desc: '',
      args: [],
    );
  }

  String get teacher {
    return Intl.message(
      'Teacher',
      name: 'teacher',
      desc: '',
      args: [],
    );
  }

  String get checkingCredentials {
    return Intl.message(
      'Checking credentials...',
      name: 'checkingCredentials',
      desc: '',
      args: [],
    );
  }

  String get couldNotCreateThisAccountLetsSeeWhetherYouAlready {
    return Intl.message(
      'Could not create this account. Let\'s see whether you already registered before...',
      name: 'couldNotCreateThisAccountLetsSeeWhetherYouAlready',
      desc: '',
      args: [],
    );
  }

  String get thereAreNoJoinTestsYet {
    return Intl.message(
      'There are no Join Tests yet.',
      name: 'thereAreNoJoinTestsYet',
      desc: '',
      args: [],
    );
  }

  String get createNewJoinTest {
    return Intl.message(
      'Create new Join Test',
      name: 'createNewJoinTest',
      desc: '',
      args: [],
    );
  }

  String get joinid {
    return Intl.message(
      'JoinId',
      name: 'joinid',
      desc: '',
      args: [],
    );
  }

  String get ifYouDontHaveAJoinidPleaseAskYourTeacher {
    return Intl.message(
      'If you don\'t have a JoinId, please ask your teacher.',
      name: 'ifYouDontHaveAJoinidPleaseAskYourTeacher',
      desc: '',
      args: [],
    );
  }

  String get ooopsThisJoinidIsNotValidForYou {
    return Intl.message(
      'Ooops, this JoinId is not valid for you.',
      name: 'ooopsThisJoinidIsNotValidForYou',
      desc: '',
      args: [],
    );
  }

  String get presortedAndFocusedView {
    return Intl.message(
      'Pre-sorted and focused view',
      name: 'presortedAndFocusedView',
      desc: '',
      args: [],
    );
  }

  String get useSimplifiedView {
    return Intl.message(
      'Use simplified view',
      name: 'useSimplifiedView',
      desc: '',
      args: [],
    );
  }

  String get printableContinuousText {
    return Intl.message(
      'Printable continuous text',
      name: 'printableContinuousText',
      desc: '',
      args: [],
    );
  }

  String get viewRaw {
    return Intl.message(
      'View raw',
      name: 'viewRaw',
      desc: '',
      args: [],
    );
  }

  String get legalNotice {
    return Intl.message(
      'Legal notice',
      name: 'legalNotice',
      desc: '',
      args: [],
    );
  }

  String get legalNoticeAccordingToGermanLegalNoticeLaws {
    return Intl.message(
      'Impressum gemäß der deutschen Impressumspflicht  Verantwortlicher und Seitenbetreiber ist:',
      name: 'legalNoticeAccordingToGermanLegalNoticeLaws',
      desc: '',
      args: [],
    );
  }

  String get mail {
    return Intl.message(
      'Mail',
      name: 'mail',
      desc: '',
      args: [],
    );
  }

  String get phone {
    return Intl.message(
      'Phone',
      name: 'phone',
      desc: '',
      args: [],
    );
  }

  String get mailUs {
    return Intl.message(
      'Mail us',
      name: 'mailUs',
      desc: '',
      args: [],
    );
  }

  String get callUs {
    return Intl.message(
      'Call us',
      name: 'callUs',
      desc: '',
      args: [],
    );
  }

  String get privacyPolicy {
    return Intl.message(
      'Datenschutzerklärung',
      name: 'privacyPolicy',
      desc: '',
      args: [],
    );
  }

  String get vocabulary {
    return Intl.message(
      'Vocabulary',
      name: 'vocabulary',
      desc: '',
      args: [],
    );
  }

  String get exerciseStackEdit {
    return Intl.message(
      'Exercise Stack Edit',
      name: 'exerciseStackEdit',
      desc: '',
      args: [],
    );
  }

  String get generateExercises {
    return Intl.message(
      'Generate exercises',
      name: 'generateExercises',
      desc: '',
      args: [],
    );
  }

  String get exerciseName {
    return Intl.message(
      'Exercise name',
      name: 'exerciseName',
      desc: '',
      args: [],
    );
  }

  String get exercisesNameGenitiv {
    return Intl.message(
      'Exercises\' name',
      name: 'exercisesNameGenitiv',
      desc: '',
      args: [],
    );
  }

  String get willBeAppliedToAllGeneratedExercises {
    return Intl.message(
      'Will be applied to all generated exercises',
      name: 'willBeAppliedToAllGeneratedExercises',
      desc: '',
      args: [],
    );
  }

  String get options {
    return Intl.message(
      'Options',
      name: 'options',
      desc: '',
      args: [],
    );
  }

  String get generateInBothWays {
    return Intl.message(
      'Generate in both ways',
      name: 'generateInBothWays',
      desc: '',
      args: [],
    );
  }

  String get unselectThisIfYouOnlyWantToCreateExercisesFrom {
    return Intl.message(
      'Unselect this if you only want to create exercises from Language 1 to Language 2 but not vice versa.',
      name: 'unselectThisIfYouOnlyWantToCreateExercisesFrom',
      desc: '',
      args: [],
    );
  }

  String get copyright {
    return Intl.message(
      'Copyright',
      name: 'copyright',
      desc: '',
      args: [],
    );
  }

  String get iOwnTheCopyrightOfThisExercise {
    return Intl.message(
      'I own the copyright of this exercise.',
      name: 'iOwnTheCopyrightOfThisExercise',
      desc: '',
      args: [],
    );
  }

  String get bySavingThisExerciseIAsignAllRightsOfThis {
    return Intl.message(
      'By saving this exercise, I asign all rights of this exercise to TestApp and agree that any user and thirds will be able of reusing this exercise under the terms and conditions of CC BY-SA 4.0.',
      name: 'bySavingThisExerciseIAsignAllRightsOfThis',
      desc: '',
      args: [],
    );
  }

  String get addOneMore {
    return Intl.message(
      'Add one more',
      name: 'addOneMore',
      desc: '',
      args: [],
    );
  }

  String get exercise {
    return Intl.message(
      'Exercise',
      name: 'exercise',
      desc: '',
      args: [],
    );
  }

  String get language1 {
    return Intl.message(
      'Language 1',
      name: 'language1',
      desc: '',
      args: [],
    );
  }

  String get humanreadableVersion {
    return Intl.message(
      'Human-readable version',
      name: 'humanreadableVersion',
      desc: '',
      args: [],
    );
  }

  String get language1Regex {
    return Intl.message(
      'Language 1 RegEx',
      name: 'language1Regex',
      desc: '',
      args: [],
    );
  }

  String get noDelimiterRequired {
    return Intl.message(
      'No delimiter required',
      name: 'noDelimiterRequired',
      desc: '',
      args: [],
    );
  }

  String get language2 {
    return Intl.message(
      'Language 2',
      name: 'language2',
      desc: '',
      args: [],
    );
  }

  String get language2Regex {
    return Intl.message(
      'Language 2 RegEx',
      name: 'language2Regex',
      desc: '',
      args: [],
    );
  }

  String get generated {
    return Intl.message(
      'generated',
      name: 'generated',
      desc: '',
      args: [],
    );
  }

  String get createNewExercise {
    return Intl.message(
      'Create new exercise',
      name: 'createNewExercise',
      desc: '',
      args: [],
    );
  }

  String get selectExercises {
    return Intl.message(
      'Select exercises',
      name: 'selectExercises',
      desc: '',
      args: [],
    );
  }

  String get stackEdit {
    return Intl.message(
      'Stack edit',
      name: 'stackEdit',
      desc: '',
      args: [],
    );
  }

  String get thereAreNoExercisesForThisTopic {
    return Intl.message(
      'There are no exercises for this topic.',
      name: 'thereAreNoExercisesForThisTopic',
      desc: '',
      args: [],
    );
  }

  String get editExercise {
    return Intl.message(
      'Edit exercise',
      name: 'editExercise',
      desc: '',
      args: [],
    );
  }

  String get forkThisTest {
    return Intl.message(
      'Fork this test',
      name: 'forkThisTest',
      desc: '',
      args: [],
    );
  }

  String get deleteThisJoinTest {
    return Intl.message(
      'Delete this Join Test...',
      name: 'deleteThisJoinTest',
      desc: '',
      args: [],
    );
  }

  String get editJoinTest {
    return Intl.message(
      'Edit Join Test',
      name: 'editJoinTest',
      desc: '',
      args: [],
    );
  }

  String get joinTestName {
    return Intl.message(
      'Join Test Name',
      name: 'joinTestName',
      desc: '',
      args: [],
    );
  }

  String get exercisesSelected {
    return Intl.message(
      'exercises selected.',
      name: 'exercisesSelected',
      desc: '',
      args: [],
    );
  }

  String get pickExercises {
    return Intl.message(
      'Pick exercises',
      name: 'pickExercises',
      desc: '',
      args: [],
    );
  }

  String get writeThisTest {
    return Intl.message(
      'Write this test...',
      name: 'writeThisTest',
      desc: '',
      args: [],
    );
  }

  String get saveJoinTest {
    return Intl.message(
      'Save Join Test',
      name: 'saveJoinTest',
      desc: '',
      args: [],
    );
  }

  String get pleaseFillInAllFields {
    return Intl.message(
      'Please fill in all fields.',
      name: 'pleaseFillInAllFields',
      desc: '',
      args: [],
    );
  }

  String get areYouSureToDeleteThisJoinTest {
    return Intl.message(
      'Are you sure to delete this Join Test?',
      name: 'areYouSureToDeleteThisJoinTest',
      desc: '',
      args: [],
    );
  }

  String get yes {
    return Intl.message(
      'Yes',
      name: 'yes',
      desc: '',
      args: [],
    );
  }

  String get wordClass {
    return Intl.message(
      'Class',
      name: 'wordClass',
      desc: '',
      args: [],
    );
  }

  String get testTimeInMinutes {
    return Intl.message(
      'Test time (in minutes)',
      name: 'testTimeInMinutes',
      desc: '',
      args: [],
    );
  }

  String get allowStartingTestingForInMinutes {
    return Intl.message(
      'Allow starting testing for (in minutes)',
      name: 'allowStartingTestingForInMinutes',
      desc: '',
      args: [],
    );
  }

  String get writeJoinTest {
    return Intl.message(
      'Write Join Test',
      name: 'writeJoinTest',
      desc: '',
      args: [],
    );
  }

  String get info {
    return Intl.message(
      'Info',
      name: 'info',
      desc: '',
      args: [],
    );
  }

  String get processingTime {
    return Intl.message(
      'Processing time',
      name: 'processingTime',
      desc: '',
      args: [],
    );
  }

  String get minutes {
    return Intl.message(
      'minutes',
      name: 'minutes',
      desc: '',
      args: [],
    );
  }

  String get timeLeft {
    return Intl.message(
      'Time left',
      name: 'timeLeft',
      desc: '',
      args: [],
    );
  }

  String get startPersonalTimer {
    return Intl.message(
      'Start personal timer',
      name: 'startPersonalTimer',
      desc: '',
      args: [],
    );
  }

  String get joinId {
    return Intl.message(
      'Join Id',
      name: 'joinId',
      desc: '',
      args: [],
    );
  }

  String get pleaseShareThisIdToYourStudents {
    return Intl.message(
      'Please share this id to your students.',
      name: 'pleaseShareThisIdToYourStudents',
      desc: '',
      args: [],
    );
  }

  String get finishedWritingTakeMeToClassScore {
    return Intl.message(
      'Finished writing, take me to class\' score',
      name: 'finishedWritingTakeMeToClassScore',
      desc: '',
      args: [],
    );
  }

  String get activatingTest {
    return Intl.message(
      'Activating test...',
      name: 'activatingTest',
      desc: '',
      args: [],
    );
  }

  String get answerOrderMatters {
    return Intl.message(
      'Answer order matters',
      name: 'answerOrderMatters',
      desc: '',
      args: [],
    );
  }

  String get multiplechoiseOption {
    return Intl.message(
      'Multiple-choise option',
      name: 'multiplechoiseOption',
      desc: '',
      args: [],
    );
  }

  String get important {
    return Intl.message(
      'Important:',
      name: 'important',
      desc: '',
      args: [],
    );
  }

  String get testYourRegexBeforeSavingWeRecommendTheOpensourceTool {
    return Intl.message(
      'Test your regex before saving. We recommend the open-source tool regex101.',
      name: 'testYourRegexBeforeSavingWeRecommendTheOpensourceTool',
      desc: '',
      args: [],
    );
  }

  String get regex {
    return Intl.message(
      'RegEx',
      name: 'regex',
      desc: '',
      args: [],
    );
  }

  String get editExericse {
    return Intl.message(
      'Edit exericse',
      name: 'editExericse',
      desc: '',
      args: [],
    );
  }

  String get deleteThisExercise {
    return Intl.message(
      'Delete this exercise',
      name: 'deleteThisExercise',
      desc: '',
      args: [],
    );
  }

  String get answers {
    return Intl.message(
      'Answers',
      name: 'answers',
      desc: '',
      args: [],
    );
  }

  String get multipleChoise {
    return Intl.message(
      'Multiple choise',
      name: 'multipleChoise',
      desc: '',
      args: [],
    );
  }

  String get input {
    return Intl.message(
      'Input',
      name: 'input',
      desc: '',
      args: [],
    );
  }

  String get inputUsingRegex {
    return Intl.message(
      'Input using RegEx',
      name: 'inputUsingRegex',
      desc: '',
      args: [],
    );
  }

  String get pleaseJustLeaveEmptyUnusedTextFields {
    return Intl.message(
      'Please just leave empty unused text fields.',
      name: 'pleaseJustLeaveEmptyUnusedTextFields',
      desc: '',
      args: [],
    );
  }

  String get exerciseActivation {
    return Intl.message(
      'Exercise activation',
      name: 'exerciseActivation',
      desc: '',
      args: [],
    );
  }

  String get ifYouPlanToWriteAJoinTestYouCan {
    return Intl.message(
      'If you plan to write a Join Test you can temorarily disable exercises for a period of time. They won\'t be included in any Random Test.',
      name: 'ifYouPlanToWriteAJoinTestYouCan',
      desc: '',
      args: [],
    );
  }

  String get thisExerciseIsActivatedTapToDisable {
    return Intl.message(
      'This exercise is activated. Tap to disable.',
      name: 'thisExerciseIsActivatedTapToDisable',
      desc: '',
      args: [],
    );
  }

  String get thisExerciseIsDisabledTill {
    return Intl.message(
      'This exercise is disabled till',
      name: 'thisExerciseIsDisabledTill',
      desc: '',
      args: [],
    );
  }

  String get tapToActivate {
    return Intl.message(
      'Tap to activate.',
      name: 'tapToActivate',
      desc: '',
      args: [],
    );
  }

  String get image {
    return Intl.message(
      'Image',
      name: 'image',
      desc: '',
      args: [],
    );
  }

  String get editingImagesIsNotYetSupportedOnMobilePlatforms {
    return Intl.message(
      'Editing images is not yet supported on mobile platforms.',
      name: 'editingImagesIsNotYetSupportedOnMobilePlatforms',
      desc: '',
      args: [],
    );
  }

  String get creativeCommonsUrl {
    return Intl.message(
      'https://creativecommons.org/licenses/by-sa/4.0/',
      name: 'creativeCommonsUrl',
      desc: '',
      args: [],
    );
  }

  String get saveExercise {
    return Intl.message(
      'Save exercise',
      name: 'saveExercise',
      desc: '',
      args: [],
    );
  }

  String get areYouSureToDeleteThisExercise {
    return Intl.message(
      'Are you sure to delete this Exercise?',
      name: 'areYouSureToDeleteThisExercise',
      desc: '',
      args: [],
    );
  }

  String get addANewTopic {
    return Intl.message(
      'Add a new topic',
      name: 'addANewTopic',
      desc: '',
      args: [],
    );
  }

  String get createNewTopic {
    return Intl.message(
      'Create new topic',
      name: 'createNewTopic',
      desc: '',
      args: [],
    );
  }

  String get topicName {
    return Intl.message(
      'Topic name',
      name: 'topicName',
      desc: '',
      args: [],
    );
  }

  String get subject {
    return Intl.message(
      'Subject',
      name: 'subject',
      desc: '',
      args: [],
    );
  }

  String get create {
    return Intl.message(
      'Create',
      name: 'create',
      desc: '',
      args: [],
    );
  }

  String get editClass {
    return Intl.message(
      'Edit Class',
      name: 'editClass',
      desc: '',
      args: [],
    );
  }

  String get deleteThisClass {
    return Intl.message(
      'Delete this class',
      name: 'deleteThisClass',
      desc: '',
      args: [],
    );
  }

  String get renameClass {
    return Intl.message(
      'Rename Class',
      name: 'renameClass',
      desc: '',
      args: [],
    );
  }

  String get hint {
    return Intl.message(
      'Hint:',
      name: 'hint',
      desc: '',
      args: [],
    );
  }

  String get thinkAboutTheNameSometimesItsMoreSensfulToGive {
    return Intl.message(
      'Think about the name. Sometimes it\'s more sensful to give short names like ',
      name: 'thinkAboutTheNameSometimesItsMoreSensfulToGive',
      desc: '',
      args: [],
    );
  }

  String get butAnywaySometimesYouNeedNamesLike {
    return Intl.message(
      '" but anyway, sometimes you need names like "',
      name: 'butAnywaySometimesYouNeedNamesLike',
      desc: '',
      args: [],
    );
  }

  String get exampleClassName {
    return Intl.message(
      '10b/Chemics',
      name: 'exampleClassName',
      desc: '',
      args: [],
    );
  }

  String get youShouldTalkAboutThisWithYourColleagues {
    return Intl.message(
      '". You should talk about this with your colleagues.',
      name: 'youShouldTalkAboutThisWithYourColleagues',
      desc: '',
      args: [],
    );
  }

  String get className {
    return Intl.message(
      'Class name',
      name: 'className',
      desc: '',
      args: [],
    );
  }

  String get topicsSelected {
    return Intl.message(
      'topics selected.',
      name: 'topicsSelected',
      desc: '',
      args: [],
    );
  }

  String get students {
    return Intl.message(
      'Students',
      name: 'students',
      desc: '',
      args: [],
    );
  }

  String get studentsSelected {
    return Intl.message(
      'students selected.',
      name: 'studentsSelected',
      desc: '',
      args: [],
    );
  }

  String get saveClass {
    return Intl.message(
      'Save Class',
      name: 'saveClass',
      desc: '',
      args: [],
    );
  }

  String get areYouSureToPermanentlyDeleteThisClass {
    return Intl.message(
      'Are you sure to permanently delete this class?',
      name: 'areYouSureToPermanentlyDeleteThisClass',
      desc: '',
      args: [],
    );
  }

  String get delete {
    return Intl.message(
      'Delete',
      name: 'delete',
      desc: '',
      args: [],
    );
  }

  String get noStudentsAvailableForThisLetter {
    return Intl.message(
      'No students available for this letter.',
      name: 'noStudentsAvailableForThisLetter',
      desc: '',
      args: [],
    );
  }

  String get others {
    return Intl.message(
      'Others',
      name: 'others',
      desc: '',
      args: [],
    );
  }

  String get dashboard {
    return Intl.message(
      'Dashboard',
      name: 'dashboard',
      desc: '',
      args: [],
    );
  }

  String get more {
    return Intl.message(
      'More',
      name: 'more',
      desc: '',
      args: [],
    );
  }

  String get overview {
    return Intl.message(
      'Overview',
      name: 'overview',
      desc: '',
      args: [],
    );
  }

  String get pleaseNoteOnlyTheScoreOfActivatedTopicsAndStudents {
    return Intl.message(
      'Please note: only the score of activated topics and students is shown.',
      name: 'pleaseNoteOnlyTheScoreOfActivatedTopicsAndStudents',
      desc: '',
      args: [],
    );
  }

  String get notAvailable {
    return Intl.message(
      'Not available',
      name: 'notAvailable',
      desc: '',
      args: [],
    );
  }

  String get ooopsYouDidntWriteAnyJoinTestsInThisClass {
    return Intl.message(
      'Ooops, you didn\'t write any Join Tests in this class yet.',
      name: 'ooopsYouDidntWriteAnyJoinTestsInThisClass',
      desc: '',
      args: [],
    );
  }

  String get originalTestDeleted {
    return Intl.message(
      '[Original test deleted.]',
      name: 'originalTestDeleted',
      desc: '',
      args: [],
    );
  }

  String get errorReadingDate {
    return Intl.message(
      'Error reading date.',
      name: 'errorReadingDate',
      desc: '',
      args: [],
    );
  }

  String get topic {
    return Intl.message(
      'Topic',
      name: 'topic',
      desc: '',
      args: [],
    );
  }

  String get pressForMoreDetails {
    return Intl.message(
      'Press for more details',
      name: 'pressForMoreDetails',
      desc: '',
      args: [],
    );
  }

  String get createClass {
    return Intl.message(
      'Create class',
      name: 'createClass',
      desc: '',
      args: [],
    );
  }

  String get myClasses {
    return Intl.message(
      'My Classes',
      name: 'myClasses',
      desc: '',
      args: [],
    );
  }

  String get noClassesCreatedYet {
    return Intl.message(
      'No classes created yet.',
      name: 'noClassesCreatedYet',
      desc: '',
      args: [],
    );
  }

  String get createNewClass {
    return Intl.message(
      'Create new class',
      name: 'createNewClass',
      desc: '',
      args: [],
    );
  }

  String get wrong {
    return Intl.message(
      'Wrong',
      name: 'wrong',
      desc: '',
      args: [],
    );
  }

  String get correct {
    return Intl.message(
      'Correct',
      name: 'correct',
      desc: '',
      args: [],
    );
  }

  String get score {
    return Intl.message(
      'Score',
      name: 'score',
      desc: '',
      args: [],
    );
  }

  String get aboutTestapp {
    return Intl.message(
      'About TestApp',
      name: 'aboutTestapp',
      desc: '',
      args: [],
    );
  }

  String get testappIsAnEducationalOpensourceProjectForWritingClassTests {
    return Intl.message(
      'TestApp is an educational open-source project for writing class tests digitally on smartphones or computers. We offer easy-to-use class management, individual class tests and random practice tests. Students can register individually or a hole school/class can use TestApp.',
      name: 'testappIsAnEducationalOpensourceProjectForWritingClassTests',
      desc: '',
      args: [],
    );
  }

  String get useOfTestappIsFreeForOrganizationsConditionsApplyntheSource {
    return Intl.message(
      'Use of TestApp is free. For organizations, conditions apply.\nThe source code licensed under the terms of EUPL which is compatible to the GPL 3',
      name: 'useOfTestappIsFreeForOrganizationsConditionsApplyntheSource',
      desc: '',
      args: [],
    );
  }

  String get chaw {
    return Intl.message(
      'chaw:',
      name: 'chaw',
      desc: '',
      args: [],
    );
  }

  String get niqhomLoJiqochbeQuvJilijJijatlhmoTlhinganHol {
    return Intl.message(
      'nIqHom lo’, jIQochbe’ quv jIlij jIjatlhmo’ tlhIngan Hol.',
      name: 'niqhomLoJiqochbeQuvJilijJijatlhmoTlhinganHol',
      desc: '',
      args: [],
    );
  }

  String get euplUrl {
    return Intl.message(
      'https://eupl.eu/',
      name: 'euplUrl',
      desc: '',
      args: [],
    );
  }

  String get testappForAllDevices {
    return Intl.message(
      'TestApp for all devices',
      name: 'testappForAllDevices',
      desc: '',
      args: [],
    );
  }

  String get donate {
    return Intl.message(
      'Donate',
      name: 'donate',
      desc: '',
      args: [],
    );
  }

  String get donateToTestappOpenSourceProject {
    return Intl.message(
      'Donate to TestApp open source project',
      name: 'donateToTestappOpenSourceProject',
      desc: '',
      args: [],
    );
  }

  String get helpUs {
    return Intl.message(
      'Help us!',
      name: 'helpUs',
      desc: '',
      args: [],
    );
  }

  String get forPublishingThisAppOnAndroidAndWindowsWeNeed {
    return Intl.message(
      'For publishing this app on Android and Windows we need',
      name: 'forPublishingThisAppOnAndroidAndWindowsWeNeed',
      desc: '',
      args: [],
    );
  }

  String get viewTestappOnBuyMeACoffee {
    return Intl.message(
      'View TestApp on Buy me a coffee',
      name: 'viewTestappOnBuyMeACoffee',
      desc: '',
      args: [],
    );
  }

  String get legalNoticeAndPrivacyPolicy {
    return Intl.message(
      'Legal notice and privacy policy',
      name: 'legalNoticeAndPrivacyPolicy',
      desc: '',
      args: [],
    );
  }

  String get asThisApplicationIsANoncommercialStudentProjectByA {
    return Intl.message(
      'As this application is a non-commercial student project by a German student, we only provide a German privacy policy according to the European and German laws.',
      name: 'asThisApplicationIsANoncommercialStudentProjectByA',
      desc: '',
      args: [],
    );
  }

  String get bigTest20Exercises {
    return Intl.message(
      'Big Test (20 exercises)',
      name: 'bigTest20Exercises',
      desc: '',
      args: [],
    );
  }

  String get imenseTest50Exercises {
    return Intl.message(
      'Imense Test (50 exercises)',
      name: 'imenseTest50Exercises',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale('tlh', ''), Locale('fr', ''), Locale('de', ''), Locale('en', ''),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (Locale supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}