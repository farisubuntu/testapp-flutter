import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:test_app_flutter/classes.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/style.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class EditClass extends StatefulWidget {
  final int id;
  final Map classData;

  EditClass({this.id, this.classData});

  @override
  _EditClassState createState() => _EditClassState();
}

class _EditClassState extends State<EditClass> {
  int _loadCounter = 0;
  TextEditingController _classNameController = TextEditingController();

  Map topics;
  List students;

  Set<int> _selectedTopics = Set();
  Set<int> _selectedStudents = Set();

  bool _savingClass = false;

  @override
  void initState() {
    globals.api.call('listStudents').then((data) {
      setState(() {
        students = data['response'];
        _loadCounter++;
      });
    });
    globals.api.call('listTopics').then((data) {
      List rawTopics = data['response'];
      Map subjects = Map();
      rawTopics.forEach((currentTopic) {
        if (!subjects.keys.contains(currentTopic['subject']))
          subjects[currentTopic['subject']] = List();
        subjects[currentTopic['subject']].add(currentTopic);
      });
      setState(() {
        _loadCounter++;
        topics = subjects;
      });
    });

    jsonDecode(widget.classData['topics']).forEach((topic) {
      if (topic != null && topic != '')
        _selectedTopics.add((topic is String) ? int.parse(topic) : topic);
    });
    jsonDecode(widget.classData['students']).forEach((student) {
      if (student != null && student != '')
        _selectedStudents
            .add((student is String) ? int.parse(student) : student);
    });

    _classNameController.text = widget.classData['name'];

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (ResponsiveDrawerScaffold(
      title: S.of(context).editClass,
      appBarLeading: IconButton(
        icon: Icon(Icons.close),
        onPressed: () => Navigator.of(context).pop(),
        tooltip: S.of(context).close,
      ),
      appBarActions: <Widget>[
        IconButton(
          icon: Icon(Icons.delete_forever),
          onPressed: deleteClass,
          tooltip: S.of(context).deleteThisClass,
        )
      ],
      body: (_loadCounter >= 2)
          ? ListView(
              children: <Widget>[
                TestAppCard(children: [
                  Text(S.of(context).renameClass,
                      style: Theme.of(context).textTheme.headline6),
                  RichText(
                      text: TextSpan(
                    style: TextStyle(color: Colors.black),
                    children: [
                      TextSpan(
                        text: S.of(context).hint,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      TextSpan(
                          text: S
                              .of(context)
                              .thinkAboutTheNameSometimesItsMoreSensfulToGive),
                      TextSpan(
                          text: '10b',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(
                          text:
                              S.of(context).butAnywaySometimesYouNeedNamesLike),
                      TextSpan(
                          text: S.of(context).exampleClassName,
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(
                          text: S
                              .of(context)
                              .youShouldTalkAboutThisWithYourColleagues)
                    ],
                  )),
                  TextField(
                    controller: _classNameController,
                    decoration:
                        InputDecoration(labelText: S.of(context).className),
                  )
                ]),
                TestAppCard(children: <Widget>[
                  Text(S.of(context).topics,
                      style: Theme.of(context).textTheme.headline6),
                  Text("${_selectedTopics.length} " +
                      S.of(context).topicsSelected),
                  ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (c, index) {
                      String subjectName = topics.keys.toList()[index];
                      List<Widget> children = [];
                      topics[subjectName].forEach((topic) {
                        children.add(
                          CheckboxListTile(
                            value: (_selectedTopics
                                .contains(int.parse(topic['id']))),
                            onChanged: (selected) {
                              setState(() {
                                (selected)
                                    ? _selectedTopics
                                        .add(int.parse(topic['id']))
                                    : _selectedTopics
                                        .remove(int.parse(topic['id']));
                              });
                            },
                            title: Text(topic['name']),
                          ),
                        );
                      });
                      return (ExpansionTile(
                        title: Text(subjectName),
                        children: children,
                      ));
                    },
                    itemCount: topics.keys.length,
                  )
                ]),
                TestAppCard(children: <Widget>[
                  Text(S.of(context).students,
                      style: Theme.of(context).textTheme.headline6),
                  Text("${_selectedStudents.length} " +
                      S.of(context).studentsSelected),
                  StudentList(
                      studentData: students,
                      itemBuilder: (currentStudents) => CheckboxListTile(
                          title: Text(currentStudents['name']),
                          value: (_selectedStudents
                              .contains(int.parse(currentStudents['id']))),
                          onChanged: (selected) {
                            setState(() {
                              (selected)
                                  ? _selectedStudents
                                      .add(int.parse(currentStudents['id']))
                                  : _selectedStudents
                                      .remove(int.parse(currentStudents['id']));
                            });
                          }))
                ])
              ],
            )
          : CenterProgress(),
      floatingActionButton: (!_savingClass)
          ? FloatingActionButton.extended(
              onPressed: saveClass,
              label: Text(S.of(context).saveClass),
              icon: Icon(Icons.check),
            )
          : FloatingActionButton(
              onPressed: () {},
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.white),
              ),
            ),
    ));
  }

  void saveClass() {
    setState(() {
      _savingClass = true;
    });
    globals.api.call('updateClass', {
      'class': widget.id,
      'name': _classNameController.text,
      'students': _selectedStudents.toList(),
      'topics': _selectedTopics.toList()
    }).then((data) {
      Navigator.push(context, MaterialPageRoute(builder: (c) => Classes()));
    });
  }

  void deleteClass() {
    showDialog(
        context: context,
        child: AlertDialog(
          content: Text(S.of(context).areYouSureToPermanentlyDeleteThisClass),
          actions: <Widget>[
            MaterialButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).cancel),
            ),
            MaterialButton(
              onPressed: () {
                setState(() {
                  _savingClass = true;
                });
                globals.api.call('deleteClass', {'class': widget.id}).then(
                    (data) => Navigator.of(context)
                        .push(MaterialPageRoute(builder: (c) => Classes())));
              },
              child: Text(S.of(context).delete),
            )
          ],
        ));
  }
}

class StudentList extends StatefulWidget {
  final List studentData;
  final Widget Function(Map student) itemBuilder;

  StudentList({@required this.studentData, @required this.itemBuilder});

  @override
  _StudentListState createState() => _StudentListState();
}

class _StudentListState extends State<StudentList> {
  List alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');

  @override
  Widget build(BuildContext context) {
    return (ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (c, index) {
        List studentsWithLetter;
        if (index == alphabet.length) {
          studentsWithLetter = widget.studentData.where((student) {
            return !alphabet.contains(
                (student['name'] is String && student['name'] != '')
                    ? student['name'].substring(0, 1).toUpperCase()
                    : '');
          }).toList();
        } else {
          studentsWithLetter = widget.studentData.where((student) {
            return ((student['name'] is String && student['name'] != '')
                    ? student['name'].substring(0, 1).toUpperCase()
                    : '') ==
                alphabet[index];
          }).toList();
        }
        List<Widget> children = [];
        studentsWithLetter.forEach((student) {
          children.add(widget.itemBuilder(student));
        });
        if (children.isEmpty)
          children.add(ListTile(
            leading: Icon(Icons.info),
            title: Text(S.of(context).noStudentsAvailableForThisLetter),
          ));
        return (ExpansionTile(
          title: Text((index != alphabet.length)
              ? alphabet[index]
              : S.of(context).others),
          children: children,
        ));
      },
      itemCount: alphabet.length + 1,
    ));
  }
}
