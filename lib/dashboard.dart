import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/chart.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/login.dart';
import 'package:test_app_flutter/style.dart';
import 'package:test_app_flutter/userScore.dart';

import 'api.dart';
import 'generated/l10n.dart';
import 'globals.dart' as globals;
import 'randomTest.dart';

class Dashboard extends StatefulWidget {
  Dashboard({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  var api = globals.api;
  bool _scoreLoaded = false;
  double _score = globals.score;

  @override
  void initState() {
    super.initState();
    loadScore();
    adminCheck().then(((data) {
      setState(() {});
    }));
  }

  void loadScore() {
    api.call('userScore').then((data) {
      setState(() {
        _scoreLoaded = true;
        if (!(data['response'] is bool)) {
          globals.score = data['response'] * 100;
        } else {
          globals.score = -1;
        }
        Preferences().save("score", globals.score.toString());
        _score = globals.score;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var chart = (_score != -1)
        ? (_scoreLoaded) ? new ScoreChart(_score) : CenterProgress()
        : ListTile(
            leading: Icon(Icons.info),
            title: Text(S.of(context).youDidntWriteAnyTestYet),
            trailing: OutlineButton(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (b) => RandomTest()));
              },
              child: Text(S.of(context).writeTest),
            ));
    return ResponsiveDrawerScaffold(
      body: ListView(
        children: [
          TestAppCard(
            children: <Widget>[
              chart,
              ButtonBar(
                children: <Widget>[
                  RaisedButton(
                    child: Text(S.of(context).more),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => UserScore()));
                    },
                  )
                ],
              )
            ],
          ),
          TestAppCard(children: [
            Center(child: SizedBox(height: 320, child: globals.dashboardBear))
          ])
        ],
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: S.of(context).writeTest,
        child: Icon(FontAwesomeIcons.penFancy),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => RandomTest()),
          );
        },
      ),
    );
  }
}
