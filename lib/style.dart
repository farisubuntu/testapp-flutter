import 'dart:math';

import 'package:flutter/material.dart';

import 'generated/l10n.dart';

class TestAppCard extends StatelessWidget {
  TestAppCard({Key key, this.children}) : super(key: key);

  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        constraints: BoxConstraints(maxWidth: 720),
        width: double.infinity,
        child: (Card(
          child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: children,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
              )),
        )),
      ),
    );
  }
}

Color colorByText(text) {
  var rgbString = text.hashCode.toString().substring(0, 6);
  return Color.fromRGBO(
      int.parse(rgbString.substring(0, 2)),
      int.parse(rgbString.substring(2, 4)),
      int.parse(rgbString.substring(4, 6)),
      1);
}

class RandomBear extends StatelessWidget {
  final String type;
  final int offset;
  final double scale;

  RandomBear({this.type = 'joky', this.offset, this.scale = 1});

  @override
  Widget build(BuildContext context) {
    String location = 'assets/bears/bear';
    if (['bad', 'good', 'joky', 'super'].contains(this.type)) {
      location += '-' + this.type;
      Map offsetSizes = {'bad': 6, 'joky': 14, 'good': 7, 'super': 2};
      if (this.offset != null && this.offset <= offsetSizes[this.type]) {
        location += '-' + this.offset.toString() + '.gif';
      } else {
        location += '-' +
            new Random().nextInt(offsetSizes[this.type] + 1).toString() +
            '.gif';
      }
    } else {
      location += '-standard.png';
    }
    return Image.asset(
      location,
      excludeFromSemantics: true,
      scale: this.scale,
    );
  }
}

class CenterProgress extends StatefulWidget {
  final String label;

  CenterProgress({this.label = ''});

  @override
  _CenterProgressState createState() => _CenterProgressState();
}

class _CenterProgressState extends State<CenterProgress>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<Color> _colorTween;
  TweenSequence<Color> _tweenSequence = TweenSequence([
    TweenSequenceItem(
        tween: ColorTween(begin: Colors.green, end: Colors.lightBlue),
        weight: 1),
    TweenSequenceItem(
        tween: ColorTween(begin: Colors.lightBlue, end: Colors.lightBlue),
        weight: 2),
    TweenSequenceItem(
        tween: ColorTween(begin: Colors.lightBlue, end: Colors.green),
        weight: 1),
    TweenSequenceItem(
        tween: ColorTween(begin: Colors.green, end: Colors.green), weight: 2),
  ]);

  @override
  void initState() {
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 4));
    _colorTween = _tweenSequence.animate(_animationController);
    _animationController.repeat();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _colorTween,
        builder: (context, child) => Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 48,
                    height: 48,
                    child: CircularProgressIndicator(
                      semanticsLabel: S.of(context).loadingData,
                      valueColor: _colorTween,
                    ),
                  ),
                  if (widget.label != '')
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(widget.label),
                    ),
                ],
              ),
            )));
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
