import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/api.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/editExercise.dart';
import 'package:test_app_flutter/exerciseStackEdit.dart';
import 'package:test_app_flutter/style.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class Exercises extends StatefulWidget {
  final bool selectExercises;
  final ExercisesSelection selectedExercises;

  Exercises({this.selectExercises = false, this.selectedExercises});

  @override
  _ExercisesState createState() => _ExercisesState();
}

class _ExercisesState extends State<Exercises>
    with SingleTickerProviderStateMixin {
  bool _subjectsLoaded = false;

  Map<String, List<Map>> subjectData = {};
  List subjectNames;

  TabController _tabController;

  ExercisesSelection _selectedExercises = ExercisesSelection();

  @override
  void initState() {
    super.initState();
    globals.api.call('listTopics').then((data) {
      data['response'].forEach((topic) {
        if (!subjectData.keys.contains(topic['subject']))
          subjectData[topic['subject']] = [];
        subjectData[topic['subject']]
            .add({'name': topic['name'], 'id': topic['id']});
      });

      setState(() {
        _subjectsLoaded = true;
        subjectNames = subjectData.keys.toList();

        _tabController =
            TabController(length: subjectNames.length, vsync: this);
        _tabController.addListener(_saveTabOffset);
        Preferences().fetch('exercisesTabOffset').then((index) {
          _tabController.animateTo((index != null) ? int.parse(index) : 0);
        });
      });
    });
    if (widget.selectedExercises != null)
      _selectedExercises = widget.selectedExercises;
  }

  void _saveTabOffset() async {
    Preferences().save('exercisesTabOffset', _tabController.index.toString());
  }

  @override
  Widget build(BuildContext context) {
    var fab = (!widget.selectExercises)
        ? FloatingActionButton(
            onPressed: () => Navigator.of(context)
                .push(MaterialPageRoute(builder: (b) => EditExercise())),
            tooltip: S.of(context).createNewExercise,
            child: Icon(Icons.add),
          )
        : FloatingActionButton.extended(
            onPressed: returnExercises,
            label: Text(S.of(context).selectExercises),
            icon: Icon(Icons.check),
          );
    if (_subjectsLoaded) {
      List<Widget> tabHeaders = [];
      subjectNames.forEach((name) => tabHeaders.add(Tab(
            text: name,
          )));

      List<Widget> tabBody = [];
      subjectNames.forEach((name) => tabBody.add(ExercisesTabBody(
          topics: subjectData[name],
          selectExercises: widget.selectExercises,
          selectedExercises: _selectedExercises)));

      return (ResponsiveDrawerScaffold(
        appBarBottom: TabBar(
          controller: _tabController,
          isScrollable: true,
          tabs: tabHeaders,
        ),
        appBarActions: <Widget>[
          IconButton(
            icon: Icon(FontAwesomeIcons.folderPlus),
            onPressed: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (c) => ExerciseStackEdit()));
            },
            tooltip: S.of(context).stackEdit,
          )
        ],
        title: (widget.selectExercises)
            ? S.of(context).selectExercises
            : 'TestApp',
        appBarLeading: (widget.selectExercises)
            ? IconButton(
                icon: Icon(Icons.close), onPressed: Navigator.of(context).pop)
            : null,
        body: TabBarView(
          controller: _tabController,
          children: tabBody,
        ),
        floatingActionButton: fab,
      ));
    } else {
      return (ResponsiveDrawerScaffold(
        appBarBottom: PreferredSize(
          child: Center(
              child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: (CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(Colors.white))),
          )),
          preferredSize: Size(48, 48),
        ),
        title: (widget.selectExercises)
            ? S.of(context).selectExercises
            : 'TestApp',
        appBarLeading: (widget.selectExercises)
            ? IconButton(
                icon: Icon(Icons.close), onPressed: Navigator.of(context).pop)
            : null,
        body: CenterProgress(),
        floatingActionButton: fab,
      ));
    }
  }

  void returnExercises() {
    Navigator.of(context).pop(_selectedExercises);
  }
}

class ExercisesTabBody extends StatefulWidget {
  final List topics;
  final bool selectExercises;
  final ExercisesSelection selectedExercises;

  ExercisesTabBody(
      {@required this.topics,
      this.selectExercises = false,
      this.selectedExercises});

  @override
  _ExercisesTabBodyState createState() => _ExercisesTabBodyState();
}

class _ExercisesTabBodyState extends State<ExercisesTabBody>
    with AutomaticKeepAliveClientMixin<ExercisesTabBody> {
  bool _exercisesLoaded = false;

  List _topicIds = [];
  Map _topicNamesById = {};
  List exercises;
  Map<int, List<Map>> _exercisesByTopic = {};

  List<Widget> _children = [];

  @override
  void initState() {
    super.initState();
    widget.topics.forEach((topic) {
      _topicIds.add(topic['id']);
      _topicNamesById[topic['id']] = topic['name'];
    });

    globals.api.call('fetchExercises', {'topics': _topicIds}).then((data) {
      if (data['response'] is List) {
        exercises = data['response'];

        exercises.forEach((exercise) {
          jsonDecode(exercise['topic']).forEach((topicId) {
            if (!_exercisesByTopic.keys.contains(int.parse(topicId.toString())))
              _exercisesByTopic[int.parse(topicId.toString())] = [];
            _exercisesByTopic[int.parse(topicId.toString())].add(exercise);
          });
        });
      }

      widget.topics.forEach((topic) {
        _children.add(TopicExerciseTile(
            exercises: _exercisesByTopic[int.parse(topic['id'])],
            name: topic['name'],
            selectExercises: widget.selectExercises,
            selectedExercises: widget.selectedExercises));
      });

      setState(() {
        _exercisesLoaded = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (_exercisesLoaded) {
      return Container(
        color: Colors.white,
        child: (ListView(
          children: _children,
        )),
      );
    } else {
      return (CenterProgress());
    }
  }

  @override
  bool get wantKeepAlive => true;
}

class TopicExerciseTile extends StatefulWidget {
  final List exercises;
  final String name;
  final bool selectExercises;
  final ExercisesSelection selectedExercises;

  TopicExerciseTile(
      {@required this.exercises,
      @required this.name,
      this.selectExercises = false,
      this.selectedExercises});

  @override
  _TopicExerciseTileState createState() => _TopicExerciseTileState();
}

class _TopicExerciseTileState extends State<TopicExerciseTile> {
  @override
  Widget build(BuildContext context) {
    return (ExpansionTile(
      title: Text(widget.name),
      children: <Widget>[
        (widget.exercises == null)
            ? ListTile(
                leading: Icon(Icons.info),
                title: Text(S.of(context).thereAreNoExercisesForThisTopic),
              )
            : ListView.separated(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  Widget title = RichText(
                      text: TextSpan(
                    style: TextStyle(color: Colors.black),
                    children: [
                      TextSpan(
                        text: widget.exercises[index]['name'] + ': ',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      TextSpan(text: widget.exercises[index]['content'])
                    ],
                  ));
                  int id = int.parse(widget.exercises[index]['id'].toString());
                  return ((!widget.selectExercises)
                      ? ListTile(
                          leading: Chip(label: Text(id.toString())),
                          title: title,
                          trailing: IconButton(
                            icon: Icon(Icons.edit),
                            onPressed: () =>
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (b) => EditExercise(
                                          id: id,
                                        ))),
                            tooltip: S.of(context).editExercise +
                                ' "${widget.exercises[index]['name']}"',
                          ),
                        )
                      : CheckboxListTile(
                          title: title,
                          value: widget.selectedExercises.contains(id),
                          onChanged: (selected) {
                            setState(() {
                              if (selected) {
                                widget.selectedExercises.add(id);
                              } else {
                                widget.selectedExercises.remove(id);
                              }
                            });
                          }));
                },
                separatorBuilder: (c, i) => Divider(),
                itemCount: widget.exercises.length)
      ],
    ));
  }
}

class ExercisesSelection {
  Set<int> exercises = Set();

  void addAll(Iterable exerciseIds) {
    exerciseIds.forEach((id) => this.add(int.parse(id.toString())));
  }

  void add(int id) {
    exercises.add(id);
  }

  void remove(int id) {
    exercises.remove(id);
  }

  bool contains(int id) {
    return exercises.contains(id);
  }

  String toString() {
    return exercises.toString();
  }

  Set toSet() {
    return exercises;
  }
}
