import 'package:flutter/material.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/style.dart';
import 'package:test_app_flutter/writeTest.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class RandomTest extends StatefulWidget {
  RandomTest({Key key}) : super(key: key);

  @override
  _RandomTestState createState() => _RandomTestState();
}

class _RandomTestState extends State<RandomTest> {
  var api = globals.api;
  bool _topicsLoaded = false;
  Map _topics = Map();
  var _numExercises = 5;
  var _selectedTopics = [];
  int _pageId = 0;
  var _pageController = PageController(initialPage: 0, keepPage: false);
  bool _showProgress = false;

  void loadTopics() {
    api.call('listTopics').then((data) {
      List rawTopics = data['response'];
      Map subjects = Map();
      rawTopics.forEach((currentTopic) {
        if (!subjects.keys.contains(currentTopic['subject']))
          subjects[currentTopic['subject']] = List();
        subjects[currentTopic['subject']].add(currentTopic);
      });
      setState(() {
        _topicsLoaded = true;
        _topics = subjects;
      });
    });
  }

  void _numExercisesChanged(changed) {
    setState(() {
      _numExercises = changed;
    });
  }

  void _startTest() {
    setState(() {
      _showProgress = true;
    });
    Map<String, dynamic> options = Map();
    options['topics'] = _selectedTopics;
    options['number'] = _numExercises;
    options['images'] = true;
    api.call('fetchExercises', options).then(((data) {
      setState(() {
        _showProgress = false;
      });
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => WriteTest(exercises: data['response'])),
      );
    }));
  }

  List<Map<String, dynamic>> _topicData(context) => [
        {"label": S.of(context).shortTest5Exercises, "number": 5},
        {"label": S.of(context).smartTest10Exercises, "number": 10},
        {"label": S.of(context).bigTest20Exercises, "number": 20},
        {"label": S.of(context).imenseTest50Exercises, "number": 50},
      ];

  @override
  Widget build(BuildContext context) {
    if (!_topicsLoaded) loadTopics();

    var topicList;
    if (_topicsLoaded) {
      topicList = Expanded(
        child: ListView.separated(
          itemCount: _topics.keys.length,
          itemBuilder: (BuildContext context, int subjectIndex) {
            String subjectName = _topics.keys.toList()[subjectIndex];
            List<Widget> topicTiles = List();
            _topics[subjectName].forEach((topic) {
              topicTiles.add(CheckboxListTile(
                  title: Text(topic["name"]),
                  value: _selectedTopics.contains(topic["id"]),
                  onChanged: (bool value) {
                    setState(() {
                      if (value) {
                        _selectedTopics.add(topic["id"]);
                      } else {
                        _selectedTopics.remove(topic["id"]);
                      }
                    });
                  }));
            });

            return (ExpansionTile(
              title: Text(subjectName),
              //backgroundColor: colorByText(subjectName),
              children: topicTiles,
            ));
          },
          separatorBuilder: (BuildContext context, int index) =>
              const Divider(),
        ),
      );
    } else {
      topicList = CenterProgress();
    }

    var isLastPage = (_pageId != 0);
    var nothingSelected = (_selectedTopics.length == 0);
    var fab = (!_showProgress)
        ? FloatingActionButton.extended(
            onPressed: () {
              if (!nothingSelected) {
                isLastPage
                    ? _startTest()
                    : _pageController.nextPage(
                        duration: Duration(milliseconds: 600),
                        curve: Curves.easeInOut);
              }
            },
            backgroundColor:
                nothingSelected ? Colors.grey : Theme.of(context).accentColor,
            label: Row(
              children: <Widget>[
                Text(nothingSelected
                    ? S.of(context).pleaseSelect
                    : isLastPage
                        ? S.of(context).startTest
                        : S.of(context).next),
                Icon(isLastPage ? Icons.play_arrow : Icons.navigate_next)
              ],
            ))
        : FloatingActionButton(
            onPressed: () {},
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(Colors.white),
            ),
            tooltip: S.of(context).loadingTest,
          );

    return ResponsiveDrawerScaffold(
      body: PageView(
        controller: _pageController,
        onPageChanged: (pageId) {
          setState(() {
            _pageId = pageId;
          });
        },
        children: <Widget>[
          Column(
            children: <Widget>[
              Text(
                S.of(context).topics,
                style: Theme.of(context).textTheme.headline5,
              ),
              topicList
            ],
          ),
          Column(
            children: <Widget>[
              //chart,
              Text(
                S.of(context).testType,
                style: Theme.of(context).textTheme.headline5,
              ),
              Expanded(
                  child: ListView.separated(
                itemCount: _topicData(context).length,
                itemBuilder: (BuildContext context, int index) {
                  return RadioListTile(
                      title: Text(_topicData(context)[index]["label"]),
                      value: _topicData(context)[index]["number"],
                      groupValue: _numExercises,
                      onChanged: _numExercisesChanged);
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(),
              )),
            ],
          ),
        ],
      ),
      floatingActionButton: fab,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
