import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/editClass.dart';
import 'package:test_app_flutter/style.dart';
import 'package:test_app_flutter/testScore.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'chart.dart';
import 'generated/l10n.dart';
import 'globals.dart' as globals;

class ClassScore extends StatefulWidget {
  final int classId;

  ClassScore({@required this.classId});

  @override
  _ClassScoreState createState() => _ClassScoreState();
}

class _ClassScoreState extends State<ClassScore> {
  List _topicScores = [];
  double _classScore;
  List _studentScores;
  List _joinTests;

  bool _scoreLoaded = false;
  bool _joinTestsLoaded = false;
  bool _editClassLoading = false;

  @override
  void initState() {
    super.initState();
    globals.api
        .call('classScore', {'class': widget.classId.toString()}).then((data) {
      setState(() {
        _scoreLoaded = true;
        _classScore = data['response']['score'].toDouble();
        _topicScores = data['response']['topics'];
        _studentScores = data['response']['students'];
      });
    });
    globals.api.call(
        'classJoinTests', {'class': widget.classId.toString()}).then((data) {
      if (data['response'] is List)
        _joinTests = data['response'].reversed.toList();
      setState(() {
        _joinTestsLoaded = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: ResponsiveDrawerScaffold(
          appBarBottom: TabBar(tabs: [
            Tab(
              text: S.of(context).overview,
            ),
            Tab(text: S.of(context).topics),
            Tab(
              text: S.of(context).joinTests,
            )
          ]),
          body: TabBarView(children: [
            SingleChildScrollView(
              child: Column(
                //shrinkWrap: true,
                children: <Widget>[
                  TestAppCard(
                    children: <Widget>[
                      (_scoreLoaded)
                          ? ScoreChart(_classScore * 100)
                          : CenterProgress(),
                      ListTile(
                        leading: Icon(Icons.info),
                        title: Text(S
                            .of(context)
                            .pleaseNoteOnlyTheScoreOfActivatedTopicsAndStudents),
                        trailing: OutlineButton(
                          onPressed: _editClass,
                          child: (_editClassLoading)
                              ? CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation(
                                      Theme.of(context).accentColor),
                                )
                              : Text(S.of(context).editClass),
                        ),
                      )
                    ],
                  ),
                  Container(
                    color: Colors.white,
                    child: (_scoreLoaded)
                        ? ListView.separated(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemBuilder: (context, index) {
                              var studentData = _studentScores[index];
                              return ListTile(
                                title: Text(studentData['info']['name']),
                                trailing: Text((!(studentData['score'] is bool))
                                    ? (studentData['score'] * 100)
                                            .round()
                                            .toString() +
                                        '%'
                                    : S.of(context).notAvailable),
                              );
                            },
                            separatorBuilder: (context, index) => Divider(),
                            itemCount: _studentScores.length)
                        : CenterProgress(),
                  )
                ],
              ),
            ),
            Container(
              color: Colors.white,
              child: ListView.separated(
                  itemBuilder: (context, index) {
                    if (index == 0)
                      return ListTile(
                        leading: Icon(Icons.info),
                        title: Text(S
                            .of(context)
                            .pleaseNoteOnlyTheScoreOfActivatedTopicsAndStudents),
                        trailing: OutlineButton(
                          onPressed: _editClass,
                          child: (_editClassLoading)
                              ? CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation(
                                      Theme.of(context).accentColor),
                                )
                              : Text(S.of(context).editClass),
                        ),
                      );
                    var topicData = _topicScores[index - 1]['topic'];
                    var topicScore = _topicScores[index - 1]['score'];
                    var studentScore = _topicScores[index - 1]['students'];
                    return (ExpansionTile(
                      leading: Chip(label: Text(topicData['subject'])),
                      title: Text(topicData['name']),
                      children: <Widget>[
                        TestAppCard(children: [
                          ListView.separated(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) {
                                if (index == 0)
                                  return (ListTile(
                                    leading: Icon(FontAwesomeIcons.chartPie),
                                    title: Text(topicData['name']),
                                    isThreeLine: true,
                                    subtitle: (topicScore is double ||
                                            topicScore is int)
                                        ? Container(
                                            height: 120,
                                            width: 120,
                                            child: ScoreChart(
                                              (topicScore.toDouble() * 100),
                                              labelText: 'Topic',
                                              height: 84,
                                            ),
                                          )
                                        : Text(S.of(context).noDataAvailable),
                                  ));
                                var studentData = studentScore[index - 1];
                                return (ListTile(
                                  leading: Icon(FontAwesomeIcons.solidUser),
                                  title: Text(studentData['info']['name']),
                                  trailing: Text(
                                      (studentData['score'] is double ||
                                              studentData['score'] is int)
                                          ? (studentData['score'] * 100)
                                                  .round()
                                                  .toString() +
                                              '%'
                                          : S.of(context).noDataAvailable),
                                ));
                              },
                              separatorBuilder: (context, index) => Divider(),
                              itemCount: studentScore.length + 1),
                        ])
                      ],
                    ));
                  },
                  separatorBuilder: (context, index) => Divider(),
                  itemCount: _topicScores.length + 1),
            ),
            (_joinTestsLoaded)
                ? (_joinTests.length != 0)
                    ? Container(
                        color: Colors.white,
                        child: ListView.builder(
                          itemBuilder: (context, index) => JoinTestScoreTile(
                              id: int.parse(_joinTests[index]['id']),
                              name: _joinTests[index]['joinTest']['name']
                                  .toString(),
                              timestamp: _joinTests[index]['date'].toString()),
                          itemCount: _joinTests.length,
                        ),
                      )
                    : ListTile(
                        leading: Icon(Icons.info),
                        title: Text(S
                            .of(context)
                            .ooopsYouDidntWriteAnyJoinTestsInThisClass),
                      )
                : CenterProgress()
          ]),
        ));
  }

  void _editClass() {
    setState(() {
      _editClassLoading = true;
    });
    globals.api.call('listClasses').then((data) {
      if (data['response'] is List) {
        var classData = data['response'].singleWhere((elem) {
          return elem['id'].toString() == widget.classId.toString();
        });
        Navigator.of(context).pop();
        Navigator.of(context).push(MaterialPageRoute(
            builder: (b) =>
                EditClass(id: widget.classId, classData: classData)));

        setState(() {
          _editClassLoading = false;
        });
      }
    });
  }
}

class JoinTestScoreTile extends StatefulWidget {
  final int id;
  final String timestamp;
  final String name;

  JoinTestScoreTile(
      {@required this.id, @required this.name, @required this.timestamp});

  @override
  _JoinTestScoreTileState createState() => _JoinTestScoreTileState();
}

class _JoinTestScoreTileState extends State<JoinTestScoreTile> {
  bool _testLoaded = false;
  Map _testData = Map();
  DateTime _timestamp;
  bool _timestampError = false;
  String _name;

  @override
  void initState() {
    super.initState();
    _name =
        widget.name == 'null' ? S.of(context).originalTestDeleted : widget.name;
    try {
      _timestamp = new DateTime.fromMicrosecondsSinceEpoch(
          int.parse(widget.timestamp + '000000'));
    } catch (e) {
      _timestampError = true;
    }
  }

  void loadScore() {
    globals.api.call('joinScore', {'join': widget.id.toString()}).then((data) {
      setState(() {
        if (data['response'] is Map) _testData = data['response'];
        _testLoaded = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
        leading: (!_timestampError)
            ? Chip(
                label: Text(timeago.format(_timestamp)),
              )
            : Tooltip(
                message: S.of(context).errorReadingDate,
                child: Icon(
                  Icons.error,
                  color: Colors.red,
                ),
              ),
        title: Text(_name),
        onExpansionChanged: (opened) {
          if (opened) loadScore();
        },
        children: [
          (TestAppCard(children: [
            (_testLoaded)
                ? (_testData.length != 0)
                    ? ListView.separated(
                        itemBuilder: (context, index) {
                          if (index == 0)
                            return (ListTile(
                              leading: Icon(FontAwesomeIcons.chartPie),
                              title: Text(_name),
                              isThreeLine: true,
                              subtitle: (_testData['score'] is double ||
                                      _testData['score'] is int)
                                  ? Container(
                                      height: 120,
                                      width: 120,
                                      child: ScoreChart(
                                        (_testData['score'].toDouble() * 100),
                                        labelText: S.of(context).topic,
                                        height: 84,
                                      ),
                                    )
                                  : Text(S.of(context).noDataAvailable),
                            ));
                          Map userData = _testData['tests'][index - 1];
                          double score;
                          bool scoreAvailable = true;
                          try {
                            score = double.parse(userData['score'].toString());
                          } catch (e) {
                            scoreAvailable = false;
                          }
                          return Tooltip(
                            message: S.of(context).pressForMoreDetails,
                            child: (ListTile(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (c) => TestScore(
                                          testId: int.parse(userData['id']),
                                          hideEasterEgg: true,
                                        )));
                              },
                              leading: Icon(FontAwesomeIcons.solidUser),
                              title: Text(userData['user']['name']),
                              subtitle: (scoreAvailable)
                                  ? null
                                  : Text(S.of(context).noDataAvailable),
                              trailing: (scoreAvailable)
                                  ? Text((score * 100).round().toString() + '%')
                                  : null,
                            )),
                          );
                        },
                        separatorBuilder: (context, index) => Divider(),
                        itemCount: _testData['tests'].length + 1,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                      )
                    : ListTile(
                        leading: Icon(
                          Icons.info,
                          color: Colors.orange,
                        ),
                        title: Text(S.of(context).noDataAvailable),
                      )
                : CenterProgress(),
          ]))
        ]);
  }
}
