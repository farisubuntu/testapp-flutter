import 'package:flutter/material.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/style.dart';

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class SchoolManagement extends StatefulWidget {
  SchoolManagement({
    Key key,
  }) : super(key: key);

  @override
  _SchoolManagementState createState() => _SchoolManagementState();
}

class _SchoolManagementState extends State<SchoolManagement> {
  var api = globals.api;
  List _vouchers = [];
  bool _vouchersLoaded = false;
  List _currentVoucherCodes = [];

  void listVoucher() {
    api.call('listVouchers').then((data) {
      setState(() {
        if (data['response'] is List) _vouchers = data['response'];
        _vouchersLoaded = true;
      });
    });
  }

  void createVoucher() {
    api.call('createVoucher').then((data) {
      setState(() {
        _vouchersLoaded = false;
        _currentVoucherCodes.add(data['response']['code']);
      });
    });
  }

  @override
  void initState() {
    super.initState();
    listVoucher();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> chips = [];
    _currentVoucherCodes.forEach((code) {
      chips.add(Chip(
        label: Text(code),
      ));
    });

    return ResponsiveDrawerScaffold(
      body: ListView(
        children: [
          TestAppCard(
            children: <Widget>[
              Text(
                S.of(context).teacherAccessVoucher,
                style: Theme.of(context).textTheme.headline6,
              ),
              Text(S
                  .of(context)
                  .forRegistrationTeachersNeedAVoucherVouchersCanBeIssued),
              ButtonBar(
                children: <Widget>[
                  RaisedButton(
                    child: Text(S.of(context).createNewVoucher),
                    onPressed: createVoucher,
                  )
                ],
              ),
              Row(
                children: chips,
              ),
              (_vouchersLoaded)
                  ? (_vouchers.isNotEmpty)
                      ? ListView.separated(
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            return (ListTile(
                              title: Text(_vouchers[index]['code']),
                              trailing:
                                  Text(_vouchers[index]['expiry'].toString()),
                            ));
                          },
                          separatorBuilder: (context, index) {
                            return (Divider());
                          },
                          itemCount: _vouchers.length)
                      : ListTile(
                          leading: Icon(Icons.info),
                          title: Text(S.of(context).thereAreNoVouchersYet),
                        )
                  : CenterProgress()
            ],
          )
        ],
      ),
    );
  }
}
