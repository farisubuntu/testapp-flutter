import 'package:flutter/foundation.dart'
    show debugDefaultTargetPlatformOverride, kIsWeb;
import 'package:flutter/material.dart';
import 'package:test_app_flutter/slpashScreen.dart';
import 'package:universal_io/io.dart';

import 'generated/l10n.dart';

bool isAdmin = true;

void main() {
  if (!kIsWeb) if (Platform.isLinux || Platform.isWindows) {
    debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
  }
  runApp(new TestApp());
}

class TestApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TestApp',
      localizationsDelegates: [S.delegate],
      supportedLocales: S.delegate.supportedLocales,
      theme: ThemeData(
        primaryColor: Colors.lightBlue,
        primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),
        buttonTheme: ButtonThemeData(
            textTheme: ButtonTextTheme.primary,
            buttonColor: Colors.green,
            colorScheme: ColorScheme.light(primary: Colors.green)),
        tabBarTheme: TabBarTheme(
            labelColor: Colors.white,
            indicator: UnderlineTabIndicator(
                borderSide: BorderSide(width: 4, color: Colors.green))),
        primaryIconTheme: IconThemeData(color: Colors.white),
        accentColor: Colors.green,
        textTheme: TextTheme(
          headline6: TextStyle(fontFamily: 'PT Sans Narrow'),
          headline5: TextStyle(fontFamily: 'PT Sans Narrow'),
          headline4: TextStyle(fontFamily: 'Poiret One'),
          headline2: TextStyle(fontFamily: 'Poiret One'),
          headline1: TextStyle(fontFamily: 'Poiret One'),
        ),
        appBarTheme: AppBarTheme(
          textTheme: TextTheme(
            headline6: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w600,
                fontSize: 20,
                letterSpacing: 0.15),
          ),
        ),
        cardTheme: CardTheme(
            margin: EdgeInsets.fromLTRB(32, 16, 32, 16),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            elevation: 1),
        fontFamily: 'Montserrat',
      ),
      home: SplashScreen(),
    );
  }
}
