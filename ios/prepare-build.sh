#!/bin/bash
# leftover xcode compilation files
rm -rf $HOME/Library/Developer/Xcode/DerivedData/*
rm -rf $HOME/Library/Developer/Xcode/DerivedData/*
rm -rf $HOME/Library/Developer/Xcode/DerivedData/*

# destroy entire flutter cache (will be redownloaded and rebuilt)
rm -rf $HOME/Library/Flutter/bin/cache/*

# sometimes Flutter doesn't recompile the frameworks
rm -rf ios/Flutter/App.framework ios/Flutter/Flutter.framework

# remove the entire pub-cache
rm -rf ~/.pub-cache/*

# now, remove the build directory
rm -rf build

# now remove the .packages file
rm -f .packages

# now remove the plugins directory
rm -rf .flutter-plugins

pushd ios
pod deintegrate
rm -rf Pods Podfile.lock
rm -rf .symlinks/*
popd

flutter packages get
